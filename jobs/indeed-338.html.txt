Linux System Administrator - Telmate LLC. - San Francisco, CA - 06-15-2015 | Dice.com
Toggle navigation
Dashboard
Search
Tech News
Post Jobs
Sign In
Linux System Administrator
Telmate LLC. ,
San Francisco, CA
Posted 15 hours ago
Save
Email
Share
Toggle
Dropdown
Email
unix, linux, "amazon web services", AWS, cisco, chef
Full Time
Salary, Benefits etc
Travel not required
Telecommuting not available
Job Description
The Linux System Administrator is a critical member of the production infrastructure team at Telmate.  The System Administrator works on a team with other System Administrators, software developers, and hardware engineers to both build new pieces of highly available infrastructure, as well as maintain the existing infrastructure.    The role requires the System Administrator to have The skills and abilities to rapidly troubleshoot problems in systems and services in Linux, as well as hardware issues that could be causing performance problems. The skills and abilities to conceptualize and architect highly available redundant pieces of infrastructure as it relates to new projects entering production and the scaling of legacy systems which have been in production for many years.    Essential Functions: Unix/Linux Administration Systems Security Amazon Web Services (S3, EC2, RDS) administration Programming and Scripting in Perl or Python, and Bash Remote Access (OpenSWAN, OpenVPN, IPSEC, GRE/IPSEC, etc) System Deployment & Risk Assessment Troubleshooting & Systems monitoring Architecture Review Policies Package Management Hardening & Monitoring Forensics, including root cause analysis via tools like strace, netstat, ngrep, iostat, mpstat, ps, pmap, lsof, etc Cisco routers and switches PC based servers (Dell Servers, etc) Network Switches (HP/Extreme) Firewall Maintenance Distributed File System Maintenance (OpenAFS, Swift Object Store) Documentation Chef   Other Duties:   Reading, writing and editing system, architecture, processes and other documentation in documentation tool Reading, responding to, and writing service tickets in ticketing system Reviewing and responding to system alerts in internal and external alerting and metrics systems Strong organizational and coordination skills along with multi-tasking capabilities to get things done in a fast paced environment Ability to respond to after hours issues based on escalation Good teaching and coaching skills Professional attitude and demeanor Ability to work in a collaborative, Scrum team environment     Required Education and Experience:   Expert understanding of CentOS, and Ubuntu Strong understanding of IP Networks and Protocols Expert understanding of Shell Scripting / Linux command line Strong understanding of hardware issue troubleshooting Experience in building and maintaining highly-available enterprise software tools Solid understanding of the software development lifecycle (SDLC) Experience in the infrastructure required to serve web applications Experience in Agile Scrum software development Excellent communication skills including written and spoken English with high attention to detail Experience in Chef & Jenkins Experience with AWS     Desired Education, Experience, and Skills: Experience in Security tools such as OpenVAS Experience in the tools and processes to build a highly availability web application at scale
Posted By
Angelo Caruana
San Francisco, CA
Telmate LLC.,
Dice Id : RTX155f94
Position Id : 073871
Similar Positions
Linux System Administrator
Ginkgo LLC
San Francisco, CA
System Admin
Net2Source Inc.
San Francisco, CA
Linux Server Administrator
First Republic Bank
San Francisco, CA
View more jobs like this
Linux System Administrator-152799
Manpower
Fremont, CA
System Admin with scripting and Ruby/Perl
Compworldwide
Pleasanton, CA
Sr. Linux Systems Administrator / Operations Engineer
Bay Area Techworkers, Inc.
Palo Alto, CA
Linux Systems Administrator
The Armada Group
Cupertino, CA
Linux Admin at San Fran CA
Feuji Inc
San Francisco, CA
Linux System Admin Chef/Puppet for $75M Funded Co moving to IPO
KirkSearch
Menlo Park, CA
View less jobs like this
×
Email Job
Your Email
Recipient's Email
CC this e-mail to me
Message
Send
Dice
Learn About Dice
About Us
Company Profile
Contact Sales
Social Recruiting
Work at Dice
Privacy Policy
Contact Us
Dice Services
The Dice Report
Open Web
Browse Jobs
Skills Center
Dice Sites
Slashdot
SourceForge
The IT Job Board
Dice Everywhere
Twitter
Facebook
Google Plus
App Store
Google Play
Copyright ©1990 - 2015 Dice. All rights reserved. Use of this site is subject to certain
Terms and Conditions .
Dice is a
DHI Service
×
It's a little rough around the edges. If you find things we could do better, let us know at
newdicesupport@dice.com
or give us a call at 888-321-DICE (3423)
Continue with the new site