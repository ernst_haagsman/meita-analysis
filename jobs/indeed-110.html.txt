BI Technical Architect - Net Matrix Solutions - San Francisco, CA - 06-16-2015 | Dice.com
Toggle navigation
Dashboard
Search
Tech News
Post Jobs
Sign In
BI Technical Architect
Net Matrix Solutions ,
San Francisco, CA
Posted 33 minutes ago
Save
Email
Share
Toggle
Dropdown
Email
10+ years in the BI space and 5+ years as an BI Technical Architect. and Hi-Tech domain and Agile/scrum and INFA, BO, and source systems being (SFDC, SIEBEL & SAP)
Contract Corp-To-Corp, Contract W2
market
Travel not required
Telecommuting not available
Job Description
Net Matrix Solutions, Inc. is the leading IT professional services provider with presence in North America, Asia and Europe.. We are looking for a BI Technical Architect for 3-6 months Contract Position with our client in Greater San Francisco. Please review the job description and submit your updated resume. BI Technical Architect
Greater San Francisco 3-6 Months Job Description: Looking for BI Technical Architect, who can assist with the BI Get Well/Remediation Project Lead and provide Technical guidance to the team Review current BI Solution (Traditional DW, ODS, BO/QV) Architecture and provide recommendation & solution remediation’s to remove bottle necks, increase stability. In additional to looking at immediate and short term remediation solutions, we also want the individual to partner with our Enterprise Application Architecture team to understand the BI vision and suggest tools and future improvements that can transform the legacy DW solutions to a future state architecture that is more aligned with our vision. Experience with Software Industry/Hi-Tech domain Experience working in an Agile/Scrum environment. Experience with tools like INFA, BO, and source systems being (SFDC, SIEBEL & SAP) Need to have overall 15+ years of experience and with over 10+ years in the BI space and 5+ years as an BI Technical Architect. Extensive experience working as a BI Architect, someone who started as a BI/DW Developer, became and Architect, implementing Traditional DW applications/solutions and who is now working in the Big Data space.   You can reach me at vishalp@netmatrixsolutions.com and Phone:281-306-2943
Net Matrix Solutions,
Dice Id : 10115106
Position Id : vis_4124
Similar Positions
Business Intelligence/Business Warehouse Reporting
Kriya Software Solutions Inc
San Ramon, CA
Data Warehousing (DW) Architect | Business Intelligence (BI) Architect | BI/DW Data Analyst
VDart, Inc.
San Francisco, CA
SAP BO Consultant
Prosoft Technology Group, Inc.
San Francisco, CA
View more jobs like this
BI Technical Architect
PROCYON Technostructure
San Ramon, CA
Business Intelligence Professional Services
TRIAD Group
San Francisco, CA
BI Architect
Radiansys, Inc.
San Rafael, CA
BI Architect
Ascent
San Rafael, CA
Data Warehouse Developer
Matrix
San Francisco, CA
BPR Ops: Business Intelligence Architect BA/PM
Mindlance Inc.
Cuppertino, CA
View less jobs like this
×
Email Job
Your Email
Recipient's Email
CC this e-mail to me
Message
Send
Dice
Learn About Dice
About Us
Company Profile
Contact Sales
Social Recruiting
Work at Dice
Privacy Policy
Contact Us
Dice Services
The Dice Report
Open Web
Browse Jobs
Skills Center
Dice Sites
Slashdot
SourceForge
The IT Job Board
Dice Everywhere
Twitter
Facebook
Google Plus
App Store
Google Play
Copyright ©1990 - 2015 Dice. All rights reserved. Use of this site is subject to certain
Terms and Conditions .
Dice is a
DHI Service
×
It's a little rough around the edges. If you find things we could do better, let us know at
newdicesupport@dice.com
or give us a call at 888-321-DICE (3423)
Continue with the new site