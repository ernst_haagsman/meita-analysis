__author__ = 'eh'

import bs4
import glob
import codecs
import mysql.connector

html_files = glob.glob('devjobs/*.html')

cnx = mysql.connector.connect(user='analysis', password='1234', host='127.0.0.1', database='meita-jobs')

cursor = cnx.cursor()

add_job = ("INSERT INTO indeedjobs"
           "(pagetext, keyword) "
           "values (%(pagetext)s, 'developer');")

for file in html_files:
    with codecs.open(file, 'r', encoding='utf-8') as html_file:
        html_contents = html_file.read()
        bs = bs4.BeautifulSoup(html_contents)
        for script in bs(["script", "style"]):
            script.extract()
        text = bs.get_text(separator=' ')

        # next lines source http://stackoverflow.com/questions/22799990/beatifulsoup4-get-text-still-has-javascript
        # break into lines and remove leading and trailing space on each
        lines = (line.strip() for line in text.splitlines())
        # break multi-headlines into a line each
        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
        # drop blank lines
        text = '\n'.join(chunk for chunk in chunks if chunk)

        job_data = {
            'pagetext': text
        }

        cursor.execute(add_job, job_data)

cnx.commit()
cursor.close()
cnx.close()
