__author__ = 'eh'

import mysql.connector
import csv

cnx = mysql.connector.connect(user='analysis', password='1234', host='127.0.0.1', database='meita-jobs')

cursor = cnx.cursor()

add_job = ("INSERT INTO jobs "
           "(filename, title, description, company, address) "
           "values (%(filename)s, %(title)s, %(description)s, %(company)s, %(address)s);")

with open('jobs.csv', 'rb') as csvfile:
    jobreader = csv.reader(csvfile, delimiter=",", quotechar='"')
    for row in jobreader:
        job_data = {
            'filename' : row[0].decode('utf-8'),
            'title' : row[1].decode('utf-8'),
            'company' : row[2].decode('utf-8'),
            'description' : row[3].decode('utf-8'),
            'address' : row[4].decode('utf-8'),
        }
        cursor.execute(add_job, job_data)

cnx.commit()
cursor.close()
cnx.close()
