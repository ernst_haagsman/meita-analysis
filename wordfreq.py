__author__ = 'eh'

import nltk
import mysql.connector
from nltk.corpus import stopwords

# load jobs

cnx = mysql.connector.connect(user='analysis', password='1234', host='127.0.0.1', database='meita-jobs')
cursor = cnx.cursor()

query = "SELECT keyword, pagetext FROM indeedjobs"
cursor.execute(query)

# create texts

ea = ''
dev = ''

for (keyword, pagetext) in cursor:
    if keyword == 'ea':
        ea += pagetext + '\n'
    else:
        dev += pagetext + '\n'


# define function to tokenize and clean the strings

stopwords = stopwords.words('english')

def tokenize(string):
    tokens = nltk.RegexpTokenizer(r'[a-z]{3,}').tokenize(string.lower())
    return [w for w in tokens if w not in stopwords]

# freqdist
fd_ea = nltk.FreqDist(tokenize(ea))
fd_dev = nltk.FreqDist(tokenize(dev))

print len(fd_ea)
print len(fd_dev)

fd_ea.tabulate()
fd_dev.tabulate()

print fd_ea['teamwork']
print fd_dev['teamwork']