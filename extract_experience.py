from __future__ import print_function
__author__ = 'eh'

import mysql.connector
import util
import re
from job import Job
from spacy.en import English

# load jobs

cnx = mysql.connector.connect(user='analysis', password='1234', host='127.0.0.1', database='meita-jobs')
cursor = cnx.cursor()

query = "SELECT idjobs, filename, title, company, description, address FROM jobs"
cursor.execute(query)

# define class to store experience
class Experience:
    def __init__(self, job_id, lower, upper=0):
        self.job_id = job_id
        self.lower = lower
        self.upper = upper if upper > 0 else lower

    def __str__(self):
        return '[' + str(self.lower) + '-' + str(self.upper) + ']'

# analyze

nlp = English()
xp_phrases = []

for (idjobs, filename, title, company, description, address) in cursor:
    job = Job(idjobs, filename, title, company, description, address)
    tokens = nlp(job.description)
    # iterate over sentences
    for sentence in tokens.sents:
        # skip sentences which don't contain the word experience
        if not any(x.string.strip().lower() == u'experience' for x in sentence):
            continue

        # print any numbers from the sentence
        numbers = list([x.string.strip().lower() for x in sentence if x.tag_ == 'CD'])
        if len(numbers) > 0:
            # try to make sense of them somehow
            for number in numbers:
                if(len(number) < 1):
                    continue

                no = None
                match = re.search("(\d+)\-(\d+)", util.remove_whitespace(number))
                if match is None:
                    try:
                        no = Experience(job.id, int(number))
                    except ValueError:
                        try:
                            no = Experience(job.id, int(util.only_numbers(number)))
                        except ValueError:
                            try:
                                no = Experience(job.id, util.text2int(number))
                            except Exception:
                                pass
                else:
                    no = Experience(job.id, int(match.group(1)), int(match.group(2)))
                if no is not None:
                    xp_phrases.append(no)
                print (number, no)

# put phrases into SQL database

add_phrase = ("INSERT INTO xpphrases "
                  "(jobid, lower, upper) "
                  "values (%(jobid)s, %(lower)s, %(upper)s);")

for phrase in xp_phrases:
    phrase_data = {
        'jobid': phrase.job_id,
        'lower': phrase.lower,
        'upper': phrase.upper
    }
    cursor.execute(add_phrase, phrase_data)

cnx.commit()
cursor.close()
cnx.close()