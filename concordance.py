__author__ = 'EH'

import csv
import nltk
import sys
from nltk.text import Text

concordance_string = "legal"

job_descriptions = []

with open('jobs.csv', 'rb') as csvfile:
    jobreader = csv.reader(csvfile, delimiter=",", quotechar='"')
    for row in jobreader:
        job_descriptions.append(row[3].decode('utf-8'))

descriptions = '\n'.join(job_descriptions)

tokens = nltk.word_tokenize(descriptions)

textobj = Text(tokens)

textobj.concordance(concordance_string, width=200, lines=1000)
