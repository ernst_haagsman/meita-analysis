Technical Program Manager - Infrastructure Engineering job in Americas-United States-California-San Francisco with Autodesk
home
login
search
menu
close-circle
arrow-dropdown-up
arrow-dropdown
arrow-down
arrow-up
close
hide
show
info
jump-link
play
tip
external
fullscreen
share
view
arrow-cta
arrow-button
button-dropdown
button-download
facebook
youtube
twitter
tumblr
pinterest
instagram
googleplus
linkedin
email
blog
lock
pencil
alert
download
check
comments
image-carousel-arrow-right
show-thick
image-carousel-arrow-left
user-profile
file-upload-drag
return
cta-go-arrow-circle
circle
circle-o
circle-o-thin
square
square-o
square-o-thin
triangle
triangle-o
triangle-o-thin
square-rounded
square-rounded-o
square-rounded-o-thin
cta-go-arrow
alert-exclamation
close-thick
hide-thick
education-students
globe-international
cloud
sign-in
sign-out
target-audience
class-materials
filter
description
key-learning
pdf-file
ppt-file
zip-file
Worldwide Sites
Where applicable, you can see country-specific product information, offers, and pricing.
Change country/language
X
Search
Sign in
My Account
Autodesk Account
(includes all Subscriptions)
A360
Education Community
Education Community
Need help? Visit our sign-in FAQs
Sign out Menu
Products
Top products
AutoCAD
AutoCAD
AutoCAD LT
AutoCAD 360
AutoCAD Design Suite
All AutoCAD
3ds Max
Building Design Suite
AutoCAD Civil 3D
Inventor
Maya
Navisworks
Product Design Suite
Revit
SketchBook Pro
All products & suites
File viewers
DWG
DWF
Online viewer
All viewers
Solutions
Industry solutions
Simulation
Building Information Modeling (BIM)
All solutions
Cloud & collaboration
Makers & indie designers
3D printing
Indie game maker
Makers
Support & learning
Support
By product
System requirements
Updates, hotfixes & service packs
Customer service
Installation, activation, licensing
Account management
All customer service
Events & Training
Classes on demand
Events
Training & certification
Community resources
Blogs
Developer network
Forums
Students & educators
All communities
Services
Consulting
Partner products & services
Downloads
Free product trials
Free student software
File viewers
Updates & service packs
Buy
Buy
Buy online
Find a reseller
Special offers
Subscription plans
Support offerings
Options
Education
Financing
About Autodesk
Careers
Company
About us
Newsroom
Autodesk Labs
Investor relations
Trust center
Free trials
Students and educators
Worldwide sites
Shares:
0
0
0
Embed
Technical Program Manager - Infrastructure Engineering
Americas-United States-California-San Francisco
Description   Why Autodesk?
Are you looking for a career where you can collaborate with both creative and engineering types to build a better world with innovative technology?  If so, meet Autodesk.  Learn more about why Autodesk is continually ranked a top place to work by Fortune, Forbes, Glassdoor and others: Top 7 Reasons You Should Work at Autodesk.  And check out what Autodesk employees are saying.
As a global leader in 3D design, engineering, and entertainment software, Autodesk helps people imagine, design, and create a better world. Autodesk accelerates better design through an unparalleled depth of experience and a broad portfolio of software to give customers the power to solve their design, business, and environmental challenges. In addition to designers, architects, engineers, and media and entertainment professionals, Autodesk helps students, educators, and creators unlock their creative ideas through user-friendly applications.
Who We Are Looking For:
We are a global IT engineering organization that is looking for an experienced IT program leader who can join our leadership team and help us accelerate the value we deliver to Autodesk. We have a broad portfolio of globally deployed IT solutions spanning enterprise cloud, security, virtual hosting, collaboration, software-defined infrastructure, service automation, infrastructure software, and productivity services.
This position reports directly to the Director of Infrastructure Engineering. In this role you will be part of a leadership team responsible for directing the efforts of an 80 person global engineering organization comprised of infrastructure and software engineers.
Your role will span from driving process improvements to leading particularly complex projects/programs. You’ll have a fantastic opportunity to use your extensive IT experience while shaping technical strategy and brainstorming with technical leaders.
This is a hands on role that requires the ability to quickly summarize what’s important from complex technical discussions and present to stakeholders and executives.
Specific responsibilities:
• Provide thought leadership on engineering program execution and delivery approach.
• Develop program plans, conduct cost/benefit and risk analyses, produce contingency options, and recommend courses of action.
• Optimize execution with a comprehensive and tailored approach that integrates best practices while adapting to the specific dynamics of multiple fast moving technology teams.
• Lead efforts with the Director of Infrastructure Engineering and organization leaders to define and refine KPI’s for increasing engineering performance.
• Lead complex IT programs that span multiple teams and partner organizations.
• Be the proactive communication interface collaborating with business partners, planning and operations organizations.
• Provide up to date visibility via self-service on engineering execution including roadmap, status of key deliverables and engineering artifacts.
• Provide project management assistance and manage project coordinators or contingent resources as needed.
Requirements:
• B.S. in a technical discipline or equivalent experience.
• At least four years of systems/IT engineering or similar experience.
• At least 5 years project/program management experience with larger scale ($6M+) technology initiatives.
• Strong communication skills and experience working with highly technical management teams.
• Strong organizational and coordination skills along with multi-tasking capabilities to get things done in a fast paced environment.
• Ability to motivate and focus a large dispersed global technical team to reach challenging goals.
• Strong analytical and problem-solving skills, exposure to large-scale systems.   #LI-POST
Apply
Apply
Description   Why Autodesk?
Are you looking for a career where you can collaborate with both creative and engineering types to build a better world with innovative technology?  If so, meet Autodesk.  Learn more about why Autodesk is continually ranked a top place to work by Fortune, Forbes, Glassdoor and others: Top 7 Reasons You Should Work at Autodesk.  And check out what Autodesk employees are saying.
As a global leader in 3D design, engineering, and entertainment software, Autodesk helps people imagine, design, and create a better world. Autodesk accelerates better design through an unparalleled depth of experience and a broad portfolio of software to give customers the power to solve their design, business, and environmental challenges. In addition to designers, architects, engineers, and media and entertainment professionals, Autodesk helps students, educators, and creators unlock their creative ideas through user-friendly applications.
Who We Are Looking For:
We are a global IT engineering organization that is looking for an experienced IT program leader who can join our leadership team and help us accelerate the value we deliver to Autodesk. We have a broad portfolio of globally deployed IT solutions spanning enterprise cloud, security, virtual hosting, collaboration, software-defined infrastructure, service automation, infrastructure software, and productivity services.
This position reports directly to the Director of Infrastructure Engineering. In this role you will be part of a leadership team responsible for directing the efforts of an 80 person global engineering organization comprised of infrastructure and software engineers.
Your role will span from driving process improvements to leading particularly complex projects/programs. You’ll have a fantastic opportunity to use your extensive IT experience while shaping technical strategy and brainstorming with technical leaders.
This is a hands on role that requires the ability to quickly summarize what’s important from complex technical discussions and present to stakeholders and executives.
Specific responsibilities:
• Provide thought leadership on engineering program execution and delivery approach.
• Develop program plans, conduct cost/benefit and risk analyses, produce contingency options, and recommend courses of action.
• Optimize execution with a comprehensive and tailored approach that integrates best practices while adapting to the specific dynamics of multiple fast moving technology teams.
• Lead efforts with the Director of Infrastructure Engineering and organization leaders to define and refine KPI’s for increasing engineering performance.
• Lead complex IT programs that span multiple teams and partner organizations.
• Be the proactive communication interface collaborating with business partners, planning and operations organizations.
• Provide up to date visibility via self-service on engineering execution including roadmap, status of key deliverables and engineering artifacts.
• Provide project management assistance and manage project coordinators or contingent resources as needed.
Requirements:
• B.S. in a technical discipline or equivalent experience.
• At least four years of systems/IT engineering or similar experience.
• At least 5 years project/program management experience with larger scale ($6M+) technology initiatives.
• Strong communication skills and experience working with highly technical management teams.
• Strong organizational and coordination skills along with multi-tasking capabilities to get things done in a fast paced environment.
• Ability to motivate and focus a large dispersed global technical team to reach challenging goals.
• Strong analytical and problem-solving skills, exposure to large-scale systems.   #LI-POST
Apply
Anonymous
Linkedin
+ Comment
What is this?
Andrew Levy
Senior Manager, Social Media & Talent Brand at Autodesk
We welcome your comments and questions about this Technical Program Manager - Infrastructure Engineering opportunity at Autodesk.
×
Grab The Code!
<iframe src="http://app.ongig.com/embed/view/19530" width="450px" height="300px" frameborder="0"></iframe>
×
Oooops! This is embarrassing
There is a problem with our sign in process right now.
We hope to have it resolved shortly
×
Login to Follow This Job
×
Login to Unfollow This Job
×
Are You Done Applying?
Yes
No
×
Do You Want to Follow this Job?
Yes
No
×
Share On LinkedIn?
Share
No Thanks
×
Apply Now
Technical Program Manager - Infrastructure Engineering
Americas-United States-California-San Francisco
(Your LinkedIn profile will be included with your intro letter)
Close
Apply
×
Unsubscribe
×
There was an issue unsubscribing
×
You have been unsubscribed
Close
Unsubscribe
×
Delete Comment
×
Oh snap! You got an error responding to this comment!
DELETE PERMANENTLY
FOLLOW AUTODESK
Facebook
Twitter
LinkedIn
All social media
Products
Free product trials
3D CAD software
3D printing
Civil engineering
Construction
Drafting
Manufacturing
Sketching & painting
Student downloads
Find by industry
Support & Learning
Product support
Installation, registration & licensing
Classes on demand
Events
Updates & service packs
System requirements
Help forums
Buy
Subscription options
Autodesk Store
Find a reseller
Support offerings
Autodesk
Autodesk is a leader in 3D design, engineering and entertainment software.
About us
Careers
Contact us
Investor relations
Trust center
Newsroom
Privacy/Cookies (Updated)
|
Legal Notices & Trademarks
|
Report Noncompliance
|
Site map
| © 2015 Autodesk Inc. All rights reserved