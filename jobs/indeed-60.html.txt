Cloud Storage Architect Job in US - California - San Francisco (HQ), US - Virginia - Herndon,
Technology - Infrastructure Engineering Career,
Jobs in Salesforce
Sign in
Candidate registration
Candidate registration
Login
Forgot password
Forgot password
Sign in
Candidate registration
1-800-NO-SOFTWARE  |  1-800-667-6389  |  Contact
1-800-NO-SOFTWARE  |  1-800-667-6389  |  Contact
Login
Menu
Products
Product Overview
Sales Cloud Sales force automation & CRM
Service Cloud Customer service, support, & help desk
Marketing Cloud Social media monitoring & marketing
Salesforce Platform Cloud application development
Salesforce Chatter Enterprise social network
Salesforce Work.com Sales performance management
Editions & Pricing
Products for Small Businesses
Community
Customer Community
Developer Community
Partner Community
AppExchange Marketplace
Industries
Overview
Communications
Financial Services
Government
Healthcare & Life Sciences
High Tech
Manufacturing
Media
Nonprofit & Higher Ed
Retail
Services
Support
Customer Support
Training
Training Options
Certification
Find a Class
Consulting
Expert Consultants
Find a Partner
Find a Developer
Customers
Customer Success Stories
Events
Overview
Dreamforce
Developer Events
Webinars
Event Videos
About Us
Overview
Awards
Blogs
Careers
Leadership
Press and News
Investor Information
Global Offices
Foundation
Sustainability
Trust
Legal Information
Careers
‹
Back to Careers Home
What:
Where:
Advanced search
Cloud Storage Architect
Share
Show me jobs like this one
Category:
Technology - Infrastructure Engineering
Location:
US - California - San Francisco (HQ)
US - Virginia - Herndon
Post Date:
06/15/2015
As an Cloud Storage Architect at Salesforce.com, you will be part of a dynamic, global team designing, delivering and supporting automated technology infrastructure to meet the substantial growth needs of the business.   The Infrastructure Engineering team is essential to development of automation, consistent and stable architecture, and the deployment of all new equipment and upgrades to hardware and software.   In this role, you will have the drive and energy to have a huge impact by providing thought leadership and driving innovation across the Storage engineering domain in collaboration with the engineers and their respective managers from other disciplines such as R&D, Site Reliability, Network, Database, Release, and Systems in delivering innovative and automated solutions in an agile, dynamic environment.    The
Cloud
Storage Architect
will function in a fast paced, exciting and challenging environment. This is an extremely hands on role that will require the technical aptitude and hands on experience to create Enterprise Architecture designs and standards documentation, as well as migration strategies. Excellent interpersonal skills are critical as this role requires input from and influence on others in various departments throughout techops and R&D organizations. This skilled professional will have the demonstrated passion, competency, experience, knowledge, and understanding of Enterprise Architecture to manage the design and standards including but not limited to implementation and support. The capacity to solve complex issues specifically in transition and migration is also critical for success in this role.   Successful candidates will be recognized as thought leaders and technical experts in the Infrastructure Engineering discipline and will have the ability to set strategy, articulate and demonstrate both deep and broad knowledge across multiple areas of infrastructure technology in support of an automated SaaS/PaaS infrastructure.   We are looking for a thought leader; an innovative and dynamic leader/technologist that thrives on efficiency by contributing to the development of Storage architecture standards, automated systems/QE processes, and other automation requirements that lead to service excellence.    Successful candidates will be someone who is not only a technology visionary, but can build a team and through that team enable the creation of architecture design packages centered around infrastructure automation, review and approve engineering design packages, ensure proper oversight for quality assurance of engineering artifacts (IDLC/DOD),  provide guidance to engineering staff throughout the build phase, reinforce adherence to architectural standards/principles for sustainability, service resiliency, and global security/compliance specific guidelines, and has the demonstrated ability to understand complex systems environments  to minimize technical exposure and risk during project delivery to production.   Position Responsibilities include:
Serve as a Subject Matter Expert (SME) in storage platforms (EMC, Netapp, Hitachi, IBM, Dell, etc) and storage technologies (HW Snapshots, Block level replication, NAS, and SAN, Software Defined Storage, etc)
Provides multi-level technical support in defining, developing and evaluating storage products to meet Cloud Computing SaaS, PaaS & IaaS needs.
Diagnose and resolve complex configuration and bottleneck issues with a  complex application & support infrastructure.
Research emerging technologies and equipment; develop technical specification for new products and services. Responsible for developing and conducting Proof of Concept testing of hardware, software and services. Resolve architecture issues and provide improved design methodologies. Be a thought leader in the datacenter architecture space, leading efforts to define, advocate, select, and ultimately deliver the salesforce.com next generation SAN/Storage architectures, partnering closely with the Director of SAN team to deliver a comprehensive strategy for salesforce.com storage infrastructure. Deliver SAN/Storage architecture documentation, standards documentation, and all typical design and architecture documentation of all new architecture and design decisions for consumption by engineering, implementation, and operations teams. Perform proof of concept and pilot implementation for all next generation hardware / software platforms. Provide input into long-range SAN/Storage requirements and operational guidelines, with a focus on meeting demands of a rapidly growing and scaling global service, with experience delivering storage orchestration and automation and also continuing to improve the performance and reliability of the SAN infrastructure and the overall service. Work closely with Salesforce R&D product owners on next generation features and their impact on the SAN/Storage. Formulating and implementing standards related to SAN/Storage hardware with respect to monitoring and management. Some travel may be required.
Education:
BS/MS Degree in Computer Science.  Advanced degree is a plus.
Required Skills:
Minimum of 7 years of experience with increasing responsibility in enterprise engineering and/or architecture. 7+ years experience with developing highly resilient multi-site, high availability and disaster recovery SAN/Storage architectures. Demonstrated ability to design and manage disaster recovery capabilities. Storage organizational skills and prior experience in a similar role as a Principal Engineer or Architect. Minimum of 5 years in a Leadership Role within an Enterprise Technology Architecture, Design, and Engineering Organization Ability to Work Independently, Collaboratively and Interchangeably Excellent written and oral communication skill with experience presenting highly conceptual designs to executives, mid-level managements, fellow architects, and peer engineers. Ability to Apply Innovative Approaches to Solving design and technical issues Demonstrated Technical Skills.
Strong knowledge of data center infrastructure desired (Storage, Networking, Server Technologies, etc).
Excellent Organizational, Interpersonal and Time Management Skills Must be detailed oriented and highly motivational and the ability to work in a collaborative environment.
Prior hands-on systems and storage administration experience is a must.
Storage performance tuning, storage performance analysis, and storage performance reporting and dash boarding. Deep understanding of Cisco and Brocade enterprise SAN infrastructure.
Specialized knowledge/skills and demonstrated proficiency with Unix and Linux.
Strong functional and working knowledge of TCP/IP & FCIP protocols is required. Proven expertise and ability developing next generation infrastructure, including requirements specification, vendor evaluation and selection, data driven POCs, load testing, automated test suites, vendor interoperability, pilot deployments, and iterative design. Candidate must have strong knowledge of one of the following programming languages: Perl, Python, JAVA, shell scripting, Ruby.
Desired skills:
Prior experience with highly scalable, reliable and resilient enterprise SAN environment supporting several PB of storage.
Prior experience with design and deployment of multi-site failover technologies.
Solid understanding of Oracle and Networking technologies is a major plus.
Technical certifications such as EMC Enterprise Storage Networking Specialist (Data Center Architect) are desirable but not required.
*LI-Y
Follow us
Action
Apply
Print
Email a friend
Popular locations:
US - California - San Francisco (HQ)   (460)  |  US - New York - New York   (97)  |  US - Indiana - Indianapolis   (78)  |  UK - London   (68)  |  US - Illinois - Chicago   (67)
Popular categories:
Sales - Account Executive   (328)  |  Technology - Software Development   (100)  |  Sales Engineering   (79)  |  Customer Success – Professional Services   (78)  |  Information Technology   (73)
Home
About us
Job search
Privacy policy
Security Statement
All Rights Reserved
Mobile version
Salesforce.com is an Equal Employment Opportunity and Affirmative Action Employer. Qualified applicants will receive consideration for employment without regard to race, color, religion, sex, sexual orientation, gender perception or identity, national origin, age, marital status, protected veteran status, or disability status.
Headhunters and recruitment agencies may not submit resumes/CVs through this Web site or directly to managers. Salesforce.com does not accept unsolicited headhunter and agency resumes. Salesforce.com will not pay fees to any third-party agency or company that does not have a signed agreement with Salesforce.com.
Accessibility  –
If you require accessibility assistance applying for open positions please contact the  Salesforce.com Recruiting Department.
Contact
Phone
Phone
Offices
Contact
Follow us
1-800-NO-SOFTWARE | 1-800-667-6389 |
Contact
|
Careers
© Copyright 2000-
salesforce.com, inc.
All rights reserved . Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Powered by
CareerMount
Privacy Statement
|
Security Statement
|
Site Map
|
Feedback
Select region
Americas
América Latina (Español)
Brasil (Português)
Canada (English)
Canada (Français)
United States (English)
Europe, Middle East and Africa
España (Español)
Deutschland (Deutsch)
France (Français)
Italia (Italiano)
Nederland (Nederlands)
Sverige (Svenska)
United Kingdom (English)
All other countries (English)
Asia Pacific
Australia (English)
India (English)
日本 (日本語)
한국 (한국어)
中国 (简体中文)
台灣 (繁體中文)
All other countries (English)