__author__ = 'eh'

import nltk
import mysql.connector
import statsmodels.stats.proportion
from nltk.corpus import stopwords

# load jobs

cnx = mysql.connector.connect(user='analysis', password='1234', host='127.0.0.1', database='meita-jobs')
cursor = cnx.cursor()

query = "SELECT keyword, pagetext FROM indeedjobs"
cursor.execute(query)

# create texts

ea = ''
dev = ''

for (keyword, pagetext) in cursor:
    if keyword == 'ea':
        ea += pagetext + '\n'
    else:
        dev += pagetext + '\n'


# define function to tokenize and clean the strings

stopwords = stopwords.words('english')

def tokenize(string):
    tokens = nltk.RegexpTokenizer(r'[a-z]{3,}').tokenize(string.lower())
    return [w for w in tokens if w not in stopwords]

ea_token = tokenize(ea)
dev_token = tokenize(dev)

# count phrases

phrases = ['process modeling',
           'junior developer',
           'leadership skills',
           'it management',
           'data management',
           'organizational design',
           'business practices',
           'creativity',
           'problem solving',
           'bachelor',
           'master',
           'phd',
           'degree',
           'mathematics',
           'sales',
           'marketing',
           'finance',
           'economics',
           'functionality',
           'customization',
           'operational excellence']


for phrase in phrases:
    ea_text = ' '.join(ea_token)
    dev_text = ' '.join(dev_token)

    ea_count = ea_text.count(phrase)
    dev_count = dev_text.count(phrase)

    expected_count = int(round((float(dev_count) / len(dev_token)) * len(ea_token)))

    chisq = statsmodels.stats.proportion.proportions_chisquare([ea_count, dev_count], [len(ea_token), len(dev_token)])

    print 'Phrase: ' + phrase
    print ('Developers: Appears {0} times in {1} tokens'.format(dev_count, len(dev_token)))
    print ('Expectation: {0} occurrences'.format(expected_count))
    print ('Enterprise Architects: Appears {0} times in {1} tokens'.format(ea_count, len(ea_token)))
    print ('Chi-square: {0}, p: {1}'.format(chisq[0], chisq[1]))
    print '\n\n'