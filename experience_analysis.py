__author__ = 'eh'

import matplotlib.pyplot as plt
import scipy.stats
import mysql.connector

# parameters

lowerbound = 1
upperbound = 10

# select data

cnx = mysql.connector.connect(user='analysis', password='1234', host='127.0.0.1', database='meita-jobs')
cursor = cnx.cursor()

query = ("SELECT max(lower), keyword, idindeedjobs FROM xpphrases "
         "inner join indeedjobs on idindeedjobs=xpphrases.indeedid "
         "where upper >= %(lowerbound)s and upper <= %(upperbound)s "
         "and lower >= %(lowerbound)s and lower <= %(upperbound)s "
         "group by keyword, idindeedjobs")

data = {
    'upperbound': upperbound,
    'lowerbound': lowerbound
}

cursor.execute(query, data)

datapoints_ea = []
datapoints_dev = []

for (point, keyword, jobid) in cursor:
    # put datapoint in correct array
    if keyword=='ea':
        datapoints_ea.append(point)
    else:
        datapoints_dev.append(point)

ea_descript = scipy.stats.describe(datapoints_ea)
dev_descript = scipy.stats.describe(datapoints_dev)
t_test = scipy.stats.ttest_ind(datapoints_dev, datapoints_ea, equal_var=False)

print 'Enterprise Architect: Mean {}, Variance {}'.format(ea_descript[2], ea_descript[3])
print 'Developers: Mean {}, Variance {}'.format(dev_descript[2], dev_descript[3])
print 'Difference: T {}, p {}'.format(t_test[0], t_test[1])

plt.hist(datapoints_ea, bins=10, color='r', histtype='step', label='Enterprise Architects')
plt.hist(datapoints_dev, bins=10, color='b', histtype='step', label='Developers')
plt.title('Years of Experience')
plt.xlabel('Years')
plt.ylabel('Occurrences')
plt.legend()
plt.show()

