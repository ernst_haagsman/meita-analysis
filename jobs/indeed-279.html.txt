Salesforce Jobs and Careers - Find your #dreamjob - Salesforce.com
1-800-NO-SOFTWARE  |  1-800-667-6389  |  Contact
1-800-NO-SOFTWARE  |  1-800-667-6389  |  Contact
Login
Menu
Salesforce
Cloud Nav
Free Trial >
Call us at
1-800-667-6389
Login
Home
Products
Sales Cloud Sales force automation and CRM
Service Cloud Customer service, support, and help desk
ExactTarget Marketing Cloud Email, mobile, social, and web marketing.
Salesforce1 Platform Cloud application development
Products Overview
Editions & Pricing
Small Business Solutions
Resources & Additional Information
Customer Community
Developer Community
Partner Community
AppExchange Marketplace
Industries
Services
Customer Success Stories
Events
About Us
Careers
Questions? Contact us.
Get your FREE 30-day trial.
Start by selecting a product:
Sales Cloud: World’s #1 CRM
Service Cloud
Salesforce1 Platform
Products
Product Overview
Sales Cloud
Sales force automation and CRM
Service Cloud
Customer service, support, and help desk
ExactTarget Marketing Cloud
Email, mobile, social, & website marketing
Salesforce1 Platform
Cloud application development
Salesforce Communities
Connect customers, partners, and employees
Pardot
B2B marketing automation
Work.com
Sales performance management
Editions & Pricing
Small Business Solutions
Community
Customer Community
Developer Community
Partner Community
AppExchange Marketplace
Industries
Overview
Financial Services
Health Care
Life Sciences
Communications
Retail
Media
Public Sector
Automotive
Higher Ed
Nonprofit
Services
Support
Customer Support
Partner Support
Training
Training Options
Certifications
Consulting
Salesforce Services
Find a Partner
Find a Developer
Customers
Customer Success Stories
Events
Overview
Dreamforce
Developer Events
Webinars
Event Videos
About Us
Overview
Awards
Blogs
Careers
Leadership
Press and News
Investor Information
Global Offices
Foundation
Sustainability
Trust
Legal Information
Job Title:
Architect
Job Category:
Industry Business Unit
Location:
US - California - San Francisco (HQ)
SF-Y:
Salesforce will consider for employment qualified applicants with criminal histories in a manner consistent with the requirements of the San Francisco Fair Chance Ordinance.
Share
|
| Email this job
Product Architect            As the Product Architect, you will be designing and coding enterprise class, innovative, scalable, secure and extensible applications for salesforce.com’s Industry Customers. You will be responsible for data modeling, designing system integrations and application components, assessing test framework, designing technical product packaging, assessing architectural risks to create release ready products. Your incredible communication and presentation skills, ability to work harmoniously within a team as a leader and a contributor will be critical to the successful delivery of the next generation salesforce.com products. You will proactively engage with cross-functional teams including Product Management, Engineering, Release Mgmt and Customers facing teams to design forward looking solutions by fostering an environment of openness, analytical thinking. You will be an influencer and energizer that enables the team to collaborate towards a joint vision for salesforce.com’s industry innovation and customer success. With this position, you can expect to be hands on with commercial/enterprise product architecture and coding. Additionally, you’ll have the uncanny ability to transform domain specific business requirements to product architecture for business process specific, regulated industries such as Healthcare, Life Sciences, Wealth Management, Banking etc.       Requirements     •    10+ years experience in a software development role for release managed products     •    3+ years of Force.com development experience     •    Deep understanding of software data modeling best practices and tools     •    Deep expertise in defining and maintaining system integration architecture     •    Experience with designing enterprise workflow, SOA, Enterprise Identity Management, API design including experience designing REST resources     •    Experience with object oriented design and implementation     •    Experience designing enterprise and web scale apps including functional and performance testing frameworks     •    Deep understanding of master data management, metadata management, enterprise systems integration design and implementation techniques     •    Experience with agile development methodology     •    Bachelor's Degree in Mathematics, Computer Science or a STEM Major Preferred Experience     •    Salesforce development or architecture certification     •    Experience with data migration strategies     •    Industry domain experience in Financial Services or Healthcare Life Science
Would you like to apply to this job?
Apply for the Architect position
Salesforce.com is an Equal Employment Opportunity and Affirmative Action Employer.
Qualified applicants will receive consideration for employment without regard to race, color, religion, sex, sexual orientation, gender perception or identity, national origin, age, marital status, protected veteran status, or disability status.
Headhunters and recruitment agencies may not submit resumes/CVs through this Web site or directly to managers. Salesforce.com does not accept unsolicited headhunter and agency resumes. Salesforce.com will not pay fees to any third-party agency or company that does not have a signed agreement with Salesforce.com.
EEO - It's the law .
Accessibility
– If you require accessibility assistance applying for open positions please contact the
Salesforce.com Recruiting Department .
Find your #dreamjob
Search jobs ›
Follow us
Careers
Questions? 1-800-667-6389   |
Contact
Contact
Phone
Phone
Offices
Contact
Follow us
1-800-NO-SOFTWARE | 1-800-667-6389 |
Contact
|
Careers
© Copyright 2000-2014 salesforce.com, inc.
All rights reserved . Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Privacy Statement
|
Security Statement
|
Site Map
Rate this page
Select region
Americas
América Latina (Español)
Brasil (Português)
Canada (English)
Canada (Français)
United States (English)
Europe, Middle East and Africa
España (Español)
Deutschland (Deutsch)
France (Français)
Italia (Italiano)
Nederland (Nederlands)
Sverige (Svenska)
United Kingdom (English)
All other countries (English)
Asia Pacific
Australia (English)
India (English)
日本 (日本語)
한국 (한국어)
中国 (简体中文)
台灣 (繁體中文)
All other countries (English)
Live Chat
Live Chat
January
February
March
April
May
June
July
August
September
October
November
December
2014
2015
2016
2017
2018
2019
2020
Sun Mon Tue Wed Thu Fri Sat
Today