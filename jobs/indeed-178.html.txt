SSE/Architects - Distributed Systems
in San Francisco, California at Raretec Consulting (Posted on 06/03/2015)
SSE/Architects - Distributed Systems
San Francisco, California
Posted in
San Francisco, California
Posted on
June, 03, 2015
Salary
$125k+Neg
Jobtype
fulltime
apply now
Job Description
Description Our clients Network Processing team develops and maintains its mission-critical core payment systems. The business applications enabled by Network Processing provide the essential functions that consumers, merchants, and financial institutions in the payment ecosystem need in order to transact payments, affect settlement of funds, manage payment risk, and manage payment disputes. We are looking for Senior Staff Software Engineers who will help develop their next generation payment platform that would fuel growth in electronic payments globally and drive financial inclusion in many parts of the world. You will help push the envelope of building highly scalable, secure, cost effective and distributed applications on commodity hardware using open source technologies. This is an organization wide initiative and will involve deep collaboration with exceptional groups of software engineers, Dev-Ops engineers, payment product specialist and enterprise architects.
Market salary, strong benifits package including annual bonus
Requirements
Job Description Join us as a Senior Staff Software Engineer or Architect in the Network Processing team if you are driven technologist and have a penchant to solve complex problems arising in building distributed and highly concurrent systems. As a Senior Staff Software Engineer in the Network Processing team, you will work with product specialists to define product features and design systems integration. You will design, develop, test and support deployment of the platform. Specific Responsibilities will include -Understand existing core payment platform features and market specific requirements -Evaluate technologies, drive consensus, create proof-of-concept and frameworks -Understand integration with multiple systems. Propose, design and develop integration layers -Architect, design and develop the platform -Contribute to implementing development process and methodologies -Collaborate with security experts and Dev-Ops to create secure application code -Support DevOps to create management/monitoring apps -Responsible for developing test framework, test cases, testing and automation -Understand and document performance requirements. Build performance test suite, conduct performance testing and tuning -Contribute to building required level of documentation for requirements, design, code and implementation process -Support and contribute to deployment and production implementation process
Qualifications
-Minimum of 8-10 years of software development experience
-Substantial experience of developing Distributed Systems
-Design and coding skills with one or more of the functional programming language is a plus: -Erlang /Clojure /Scala/Scheme/Haskell etc. -Expertise with Erlang stack is a plus (Erlang, OTP, Mnesia, Riak core) -Strong foundation in computer science, with strong competencies in data structures, algorithms and software design optimized for building highly distributed and parallelized systems -Good understanding of networking concepts, security and cryptography -Knowledge of Unix/Linux -Knowledge of building automated testing and performance testing is a plus
Posted in
San Francisco, California
Posted on
June, 03, 2015
Salary
$125k+Neg
Jobtype
fulltime
apply now
Visit Us at www.Raretec.com
If you’d like to accelerate your career growth or place yourself in a situation with more potential for upward mobility, Raretec could be right for you.
Raretec has an excellent reputation among quality employers. We place recruits with top Florida and U.S. firms that have invested in current high-end technical infrastructure. These firms need employees like you who possess a hard to find and rare expertise to maximize their technical applications.
More jobs from Raretec Consulting...
apply now
© 2009-2013 Recruitics, Inc. All rights reserved
Post your Job Here