Job Details
Skip Navigation
Sign In
to Create/ Edit your profile, Access your application status, and View saved documents and Job Search Agents
Job Details
View Job Cart (0)
Email a Friend
View My Account
Senior Responsive Web Application Developer
Job ID #:
124056
Employment Type:
Full Time - Permanent
Location(s):
CA-San Francisco
Education Preferred:
Bachelors Degree (or equivalent)
Functional Area:
Technology
Relocation Provided:
No
Travel Percentage:
25%
Share:
Facebook
LinkedIn
Twitter
Apply with
LinkedIn
Position Description
Location: San Francisco, California AIG is seeking a Senior Developer to join a world class Data Solutions team. The senior developer will be responsible for design, development and implementation of digital applications spanning web and mobile channels for Insurance Agents and Insurance Customers to interact with AIG. These digital applications will integrate with devices, sensors and Internet of things (IoT) The senior developer will work in a team of application architects and developers that will design and develop self-serviced applications, integrate business critical systems, and innovate new solutions with existing tools and technology. This is highly collaborative and creative role; candidates should be comfortable wearing many hats and have the proven experience to navigate and work through grey areas.
Position Requirements
The ideal candidate must have skills and experience to develop web and mobile applications using cutting edge technologies. Combined with managing client expectations, the candidate should be able to work on multiple projects at the same time and ensure projects are completed on time. In addition, the ideal candidate should possess the ability to:
• Adapt quickly and positively to a constantly changing business environment with tight timelines • Balance many projects and priorities in a high-pressure, fast paced environment • Work with business partners to continually develop, evolve and improve web and mobile applications • Be self-motivated, independent, proactive and an effective team player • Take on new responsibilities with high energy and eager attitude • Develop good quality web and mobile applications • Implement complex applications working within a team • Pay attention to detail to ensure all tasks and the applications have no loose ends • Stay engaged with all projects and management on a weekly basis • Provide accurate updates to management of the development status • Drive understanding with audiences of varying technical ability through superb communication skills, both written and verbal Qualifications:  5+ years of application development experience in large and complex enterprise environment  5+ years of hands-on experience in development of object oriented applications using, Java and JEE technologies  5+ years of hands-on experience in design and development of Web applications with a strong focus on visualization, design and usability with specific experience writing Web APIs or RESTful Web Services, API Management and Mobile Backend as a service (MBaaS)  3+ years of hands-on experience with client-side web technologies like HTML5, CSS3, AJAX and JavaScript frameworks and tools like Node.js, Meteor, jQuery, Backbone, Bootstrap, AngularJS, Karma, Mocha, Chai, Sinon, Jasmine and Protractor  3+ years of hands-on experience in development of mobile iOS and Android applications  3+ years Agile development experience using Scrum  Ability to design intelligent databases and write advanced SQL queries  Ability to learn new technologies and systems quickly Education: Bachelor’s Degree in Computer Science, IT, or related discipline
About Us
American International Group, Inc. (AIG), a world leader in insurance and financial services, is the leading international insurance organization with operations in more than 130 countries and jurisdictions. AIG companies serve commercial, institutional and individual customers through the most extensive worldwide property-casualty and life insurance networks of any insurer. In addition, AIG companies are leading providers of retirement services, financial services and asset management around the world. AIG's common stock is listed on the New York Stock Exchange, as well as the stock exchanges in Ireland and Tokyo. AIG is an Equal Opportunity Employer.
Back to top
View Job Cart (0)
Email a Friend
View My Account
jobDetails