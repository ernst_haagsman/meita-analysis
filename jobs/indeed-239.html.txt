Careers
Systems Engineer - Deployment
Professional Services | Emeryville, CA, United States
Systems Engineer - Deployment
Aspera is the creator of next-generation transport technologies that move the world’s data at maximum speed regardless of file size, transfer distance and network conditions. Based on its patented, Emmy® award-winning FASP™ protocol, Aspera software fully utilizes existing infrastructures to deliver the fastest, most predictable file-transfer experience. Aspera’s core technology delivers unprecedented control over bandwidth, complete security and uncompromising reliability. Organizations across a variety of industries on six continents rely on Aspera software for the business-critical transport of their digital assets.
High-profile users include James Cameron’s Lightstorm Entertainment who relied on Aspera software for moving massive visual effects files across continents during the production of Avatar, Netflix who receive all their video content directly to their cloud infrastructure via Aspera On Demand and BGI, the world’s largest genome research organization where Aspera solutions are used to enable global collaboration between scientists across the globe.
As a Systems Engineer in Deployment you will plan, execute and test customer deployments of Aspera transport, collaboration and management applications. Our ideal candidate will have provided hands-on system deployments for mission-critical systems and possess a broad and deep understanding of Linux/Windows/Mac systems software, scripting skills, strong knowledge of TCP/IP networking, and security. You will need to have excellent software system skills, including knowledge of web server administration, database, and storage technologies in Linux, Windows and Mac environments, along with the ability to manage and support complete customized developments. Great communications skills are a must for understanding customer installations, collaboratively working out the solution deployment while upholding Aspera’s high standards for maintaining outstanding customer relationships.
Additionally, you would provide remote and on-site deployment assistance for Aspera products.  You will be our expert in Aspera product usage, installation and high-availability configurations on Linux, Windows, cloud, etc. and will use your expertise to perform installations and field beta releases while acting as a liaison to Solution Architects, Sales Engineering and Technical Support. This position will require some travel.
Required Skills
• Pre-building replica versions of the customer solutions, including the OS, network and firewall configurations
• Deriving and implement test cases. Perform functional and load/performance testing
• Creating and executing implementation plans in conjunction with the customer
• Performing remote or local installation and testing depending on the customer requirements
• Working closely with the customer during the deployment and testing phases to achieve successful acceptance testing
• Executing customer deployments for production and evaluation environments
• Creating documentation of platform deployments and implementations with effective hand-over to Aspera Technical Support
Required Experience
• BS in Computer Science/Engineering (or equivalent)
• Minimum of 5 years of systems engineering and/or application development experience
• Minimum of 3 years of client-facing experience, preferably with an organization serving mission-critical operations.
• Strong hands-on knowledge of Linux and Windows system administration (networking, security, databases, Apache/IIS web servers)
• In-depth knowledge of TCP/IP protocol operation
• Strong knowledge of high availability best practices and implementation (A/P and A/A clusters, redundancy etc.)
• Strong knowledge of storage systems and hardware performance analysis
• Strong scripting skills in at least one scripting language - shell, Perl, Python, Ruby, etc.
• Broad expertise spanning the entire IT stack from platforms and OS, database, networking, Internet security and various enterprise systems
• Hands-on experience with AWS and other cloud computing platforms
• Experience in configuring and leveraging VMware (Workstation / ESXi) environments
• Solid working knowledge of directory service configuration (LDAP and Windows Active Directory)
• Software development experience in distributed web services (REST, SOAP), and web front-end technologies a plus
• Solid working knowledge of security best practices.
Why Aspera?  A chance to work at a company with cutting edge technologies and patented products. Only Aspera software fully utilizes existing infrastructures to deliver the fastest, most predictable file transfer experience! Only Aspera software fully utilizes existing infrastructures to deliver the fastest, most predictable file transfer experience! Great office location, free parking and conveniently located near two public transportation stations. Medical, dental and vision insurance for employees. Great perks: breakfast and catered lunches, unlimited coffee and snacks, ping pong table and video game room. Generous 401k match. Certified 'Green' company includes funded public transportation benefit. Tuition and conference reimbursements. Frequent social events for employees and appearances from our dog, Luke (our VP of Motivation).
<< Back to Current Openings