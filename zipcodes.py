__author__ = 'eh'

import nltk
import mysql.connector

# load jobs

cnx = mysql.connector.connect(user='analysis', password='1234', host='127.0.0.1', database='meita-jobs')
cursor = cnx.cursor()

query = "SELECT keyword, pagetext FROM indeedjobs"
cursor.execute(query)

# create texts

ea = ''
dev = ''

for (keyword, pagetext) in cursor:
    if keyword == 'ea':
        ea += pagetext + '\n'
    else:
        dev += pagetext + '\n'


# define function to tokenize and clean the strings

def tokenize(string):
    return nltk.RegexpTokenizer(r'(\w+)( \w+)?, ca\W').tokenize(string.lower())

# freqdist
fd_ea = nltk.FreqDist(tokenize(ea))
fd_dev = nltk.FreqDist(tokenize(dev))

fd_ea.tabulate()

