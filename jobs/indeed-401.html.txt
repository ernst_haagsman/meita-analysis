Program Manager, Product Communications Job in US - California - San Francisco (HQ),
Technology - Technology Program Management Career,
Jobs in Salesforce
Sign in
Candidate registration
Candidate registration
Login
Forgot password
Forgot password
Sign in
Candidate registration
1-800-NO-SOFTWARE  |  1-800-667-6389  |  Contact
1-800-NO-SOFTWARE  |  1-800-667-6389  |  Contact
Login
Menu
Products
Product Overview
Sales Cloud Sales force automation & CRM
Service Cloud Customer service, support, & help desk
Marketing Cloud Social media monitoring & marketing
Salesforce Platform Cloud application development
Salesforce Chatter Enterprise social network
Salesforce Work.com Sales performance management
Editions & Pricing
Products for Small Businesses
Community
Customer Community
Developer Community
Partner Community
AppExchange Marketplace
Industries
Overview
Communications
Financial Services
Government
Healthcare & Life Sciences
High Tech
Manufacturing
Media
Nonprofit & Higher Ed
Retail
Services
Support
Customer Support
Training
Training Options
Certification
Find a Class
Consulting
Expert Consultants
Find a Partner
Find a Developer
Customers
Customer Success Stories
Events
Overview
Dreamforce
Developer Events
Webinars
Event Videos
About Us
Overview
Awards
Blogs
Careers
Leadership
Press and News
Investor Information
Global Offices
Foundation
Sustainability
Trust
Legal Information
Careers
‹
Back to Careers Home
What:
Where:
Advanced search
Program Manager, Product Communications
Share
Show me jobs like this one
Category:
Technology - Technology Program Management
Location:
US - California - San Francisco (HQ)
Post Date:
05/11/2015
Role Description:
The Technology Communications & Readiness team of the Technology & Products (T&P) division seeks a Program Manager with excellent communication and creative problem-solving skills to lead our Product Communications strategy.
The role is primarily focused on effectively communicating product-level information to customers, including product behavior changes, feature updates and retirements, and both major and off-cycle release upgrades. The successful candidate will develop a comprehensive product communications process and plan designed to help customers successfully implement Salesforce products and services as well as provide strategic guidance and customer impact analysis for customer-facing roles and executives. They will also be a contributing member of the Product Readiness team, which defines and administers the processes and tools that shape the flow of information on new Salesforce products and features from T&P to customer-facing teams.
This role is for program managers with a passion for customer advocacy and helping customers achieve success and business growth while maintaining Salesforce's key values of trust and transparency.
Your Impact:
Develop and execute end-to-end communications campaigns for major release communications, including strategy, content development and editing, approvals, and scheduling.
Manage product change communications process for customer-facing roles, customers, and executives during off-cycle releases.
Craft succinct and clear communications to explain complex technical changes to customers.
Serve as a customer advocate within the T&P organization, anticipating customer needs and driving technology teams to formulate the right plan for executing change.
Re-architect our product communications process to identify more effective and impactful ways to communicate to all customer types.
Create templates and other communications collateral to support product communications.
Support additional communications for the Technology Communications program as needed.
Requirements:
7-10 years of experience in areas of program/project management, communications, customer account management, training/readiness.
Experience working with customers, customer-facing teams, and product organizations and with the ability to lead without direct authority.
Ability to learn and deeply understand technical information and concepts, especially as it relates to database architecture.
Excellent organizational, interpersonal, leadership, influence, and relationship-building skills.
Excellent analytical and problem solving skills.
Ability to thrive under tight deadlines with limited resources.
Exceptional writing and editing skills.
Sense of humor.
Salesforce, the Customer Success Platform and world's #1 CRM, empowers companies to connect with their customers in a whole new way. We are the fastest growing of the top 10 enterprise software companies, the World's Most Innovative Company according to Forbes, and one of Fortune's 100 Best Companies to Work For six years running. The growth, innovation, and Aloha spirit of Salesforce are driven by our incredible employees who thrive on delivering success for our customers while also finding time to give back through our 1/1/1 model, which leverages 1% of our time, equity, and product to improve communities around the world. Salesforce is a team sport, and we play to win. Join us!
*LI-Y
Follow us
Action
Apply
Print
Email a friend
Popular locations:
US - California - San Francisco (HQ)   (460)  |  US - New York - New York   (97)  |  US - Indiana - Indianapolis   (78)  |  UK - London   (68)  |  US - Illinois - Chicago   (67)
Popular categories:
Sales - Account Executive   (328)  |  Technology - Software Development   (100)  |  Sales Engineering   (79)  |  Customer Success – Professional Services   (78)  |  Information Technology   (73)
Home
About us
Job search
Privacy policy
Security Statement
All Rights Reserved
Mobile version
Salesforce.com is an Equal Employment Opportunity and Affirmative Action Employer. Qualified applicants will receive consideration for employment without regard to race, color, religion, sex, sexual orientation, gender perception or identity, national origin, age, marital status, protected veteran status, or disability status.
Headhunters and recruitment agencies may not submit resumes/CVs through this Web site or directly to managers. Salesforce.com does not accept unsolicited headhunter and agency resumes. Salesforce.com will not pay fees to any third-party agency or company that does not have a signed agreement with Salesforce.com.
Accessibility  –
If you require accessibility assistance applying for open positions please contact the  Salesforce.com Recruiting Department.
Contact
Phone
Phone
Offices
Contact
Follow us
1-800-NO-SOFTWARE | 1-800-667-6389 |
Contact
|
Careers
© Copyright 2000-
salesforce.com, inc.
All rights reserved . Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Powered by
CareerMount
Privacy Statement
|
Security Statement
|
Site Map
|
Feedback
Select region
Americas
América Latina (Español)
Brasil (Português)
Canada (English)
Canada (Français)
United States (English)
Europe, Middle East and Africa
España (Español)
Deutschland (Deutsch)
France (Français)
Italia (Italiano)
Nederland (Nederlands)
Sverige (Svenska)
United Kingdom (English)
All other countries (English)
Asia Pacific
Australia (English)
India (English)
日本 (日本語)
한국 (한국어)
中国 (简体中文)
台灣 (繁體中文)
All other countries (English)