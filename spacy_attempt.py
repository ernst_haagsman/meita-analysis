from __future__ import unicode_literals
from spacy.en import English

nlp = English()

tokens = nlp(u'10+ years of experience required')

dependents = [[(elem.string, elem.tag_) for elem in x.subtree] for x in tokens if x.string == 'experience']

print dependents