Salesforce Jobs and Careers - Find your #dreamjob - Salesforce.com
1-800-NO-SOFTWARE  |  1-800-667-6389  |  Contact
1-800-NO-SOFTWARE  |  1-800-667-6389  |  Contact
Login
Menu
Salesforce
Cloud Nav
Free Trial >
Call us at
1-800-667-6389
Login
Home
Products
Sales Cloud Sales force automation and CRM
Service Cloud Customer service, support, and help desk
ExactTarget Marketing Cloud Email, mobile, social, and web marketing.
Salesforce1 Platform Cloud application development
Products Overview
Editions & Pricing
Small Business Solutions
Resources & Additional Information
Customer Community
Developer Community
Partner Community
AppExchange Marketplace
Industries
Services
Customer Success Stories
Events
About Us
Careers
Questions? Contact us.
Get your FREE 30-day trial.
Start by selecting a product:
Sales Cloud: World’s #1 CRM
Service Cloud
Salesforce1 Platform
Products
Product Overview
Sales Cloud
Sales force automation and CRM
Service Cloud
Customer service, support, and help desk
ExactTarget Marketing Cloud
Email, mobile, social, & website marketing
Salesforce1 Platform
Cloud application development
Salesforce Communities
Connect customers, partners, and employees
Pardot
B2B marketing automation
Work.com
Sales performance management
Editions & Pricing
Small Business Solutions
Community
Customer Community
Developer Community
Partner Community
AppExchange Marketplace
Industries
Overview
Financial Services
Health Care
Life Sciences
Communications
Retail
Media
Public Sector
Automotive
Higher Ed
Nonprofit
Services
Support
Customer Support
Partner Support
Training
Training Options
Certifications
Consulting
Salesforce Services
Find a Partner
Find a Developer
Customers
Customer Success Stories
Events
Overview
Dreamforce
Developer Events
Webinars
Event Videos
About Us
Overview
Awards
Blogs
Careers
Leadership
Press and News
Investor Information
Global Offices
Foundation
Sustainability
Trust
Legal Information
Job Title:
Vice President, Platform Security Engineering
Job Category:
Technology - Software Development
Location:
US - California - San Francisco (HQ)
SF-Y:
Salesforce will consider for employment qualified applicants with criminal histories in a manner consistent with the requirements of the San Francisco Fair Chance Ordinance.
Share
|
| Email this job
Security is our #1 priority.  Nothing is more important than the security of our service and customer data.  We’re looking for an engineering leader that has demonstrated a strong ability to create and execute upon a vision and strategy across cloud services.  Large scale, cloud based technology security experience is a must for this role.  The executive leading this area will drive the technical strategy to support the scale and reliability needed for the continued growth of our customer base, the evolving security and regulatory landscape and the protection of our customer’s data against adversaries. Primary Responsibilities:
Delivering and executing a compelling technical strategy for R&D/platform security Build, grow, develop and oversee the software security engineering organization Partner with security executives across infrastructure, IT and Trust to help envision and deliver company-wide security initiatives Oversee the analysis of business requirements and the subsequent interpretation into security requirements internally and externally Evaluate, prioritize and resource security projects and programs based on input from appropriate stakeholders Establish credibility as a trusted advisor to stakeholders including customers, executives, peers, and employees Evaluate, implement, and support security-focused tools and services Review security findings and actively participate in significant security events Maintain strong knowledge of ongoing security threats, remediations and operational best practices
Qualifications:
Demonstrated success and influence in the cloud services technology security space (SaaS, PaaS)  Experience leading and executing in the security technology space as a secure software development architect and leader 7+ years of successful software engineering “leadership” at top-tier cloud companies 10+ years experience in technology security Demonstrated experience creating effective security strategies Strong influencing, communication and general interpersonal skills  Driven customer advocate Expert knowledge of secure application architectures, encryption and broader security technologies Knowledge of a broad range of attack vectors and exploits 5+ years of hands on Software Development experience Computer Science Degree
Salesforce, the Customer Success Platform and world's #1 CRM, empowers companies to connect with their customers in a whole new way. We are the fastest growing of the top 10 enterprise software companies, the World’s Most Innovative Company according to Forbes, and one of Fortune’s 100 Best Companies to Work For six years running. The growth, innovation, and Aloha spirit of Salesforce are driven by our incredible employees who thrive on delivering success for our customers while also finding time to give back through our 1/1/1 model, which leverages 1% of our time, equity, and product to improve communities around the world. Salesforce is a team sport, and we play to win. Join us! *LI-Y
Would you like to apply to this job?
Apply for the Vice President, Platform Security Engineering position
Salesforce.com is an Equal Employment Opportunity and Affirmative Action Employer.
Qualified applicants will receive consideration for employment without regard to race, color, religion, sex, sexual orientation, gender perception or identity, national origin, age, marital status, protected veteran status, or disability status.
Headhunters and recruitment agencies may not submit resumes/CVs through this Web site or directly to managers. Salesforce.com does not accept unsolicited headhunter and agency resumes. Salesforce.com will not pay fees to any third-party agency or company that does not have a signed agreement with Salesforce.com.
EEO - It's the law .
Accessibility
– If you require accessibility assistance applying for open positions please contact the
Salesforce.com Recruiting Department .
Find your #dreamjob
Search jobs ›
Follow us
Careers
Questions? 1-800-667-6389   |
Contact
Contact
Phone
Phone
Offices
Contact
Follow us
1-800-NO-SOFTWARE | 1-800-667-6389 |
Contact
|
Careers
© Copyright 2000-2014 salesforce.com, inc.
All rights reserved . Various trademarks held by their respective owners.
Salesforce.com, inc. The Landmark @ One Market, Suite 300, San Francisco, CA, 94105, United States
Privacy Statement
|
Security Statement
|
Site Map
Rate this page
Select region
Americas
América Latina (Español)
Brasil (Português)
Canada (English)
Canada (Français)
United States (English)
Europe, Middle East and Africa
España (Español)
Deutschland (Deutsch)
France (Français)
Italia (Italiano)
Nederland (Nederlands)
Sverige (Svenska)
United Kingdom (English)
All other countries (English)
Asia Pacific
Australia (English)
India (English)
日本 (日本語)
한국 (한국어)
中国 (简体中文)
台灣 (繁體中文)
All other countries (English)
Live Chat
Live Chat
January
February
March
April
May
June
July
August
September
October
November
December
2014
2015
2016
2017
2018
2019
2020
Sun Mon Tue Wed Thu Fri Sat
Today