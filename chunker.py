__author__ = 'EH'

import csv
import nltk

concordance_string = "experience"

job_descriptions = []

with open('jobs.csv', 'rb') as csvfile:
    jobreader = csv.reader(csvfile, delimiter=",", quotechar='"')
    for row in jobreader:
        job_descriptions.append(row[3].decode('utf-8'))

descriptions = '\n'.join(job_descriptions)

tokens = nltk.word_tokenize(descriptions)

grammar = r"""
    NP: {<CD|TO>+<NN.?|JJ>*}
"""

cp = nltk.RegexpParser(grammar)

tagged = cp.parse(nltk.pos_tag(tokens))

print tagged
