General Electric - Job details
Select a Language:
Select a Language
Český
Dansk
Deutsch
Español
English (United States)
Français
Italiano
Magyar
Nederlands
Norsk
polski
Português (Brasil)
Slovenčina
Suomi
Svenska
Русский
한국어
中文
日本語
For those candidates who have been directed to this page via a job board, if you'd like to view this interface in a language other than US English,
please click here .
It's important to take note of the Job Number for this position as it will assist you later in the process.
Thank you!
GE Careers displays external job postings of General Electric Company and its Affiliates around the World. Each resume/cv submission to a job posting through GE Careers will flow to the GE business or affiliate that has posted the job opening.
Home Search
openings Search
results Job
details     Help
Job details   Job 1
of 1
Apply to job
&nbsp Save to cart
Job Number
2179066
Business
GE Corporate
Business Segment
Corporate IT
About Us
GE offers a great work environment, professional development, challenging careers, and competitive compensation. GE is an
Equal Opportunity Employer . Employment decisions are made without regard to race, color, religion, national or ethnic origin, sex, sexual orientation, gender identity or expression, age, disability, protected veteran status or other characteristics protected by law.
Posted Position Title
Solutions Architect – Predix Connect
Career Level
Experienced
Function
Information Technology
Function Segment
Cloud Services
Location(s) Where Opening Is Available
United States
U.S. State, China or Canada Provinces
California
City
San Ramon
Postal Code
94583-9130
Relocation Expenses
Yes
Role Summary/Purpose
Solutions Architect to design, architect and implement global virtual network solution over worldwide partners for GE’s Industrial Internet solution to meet overall global connectivity service for Industrial Internet solution. This individual will play a lead role in initiatives focused on designing how industrial assets will be connected to cloud, secured, and interconnected with resources hosted in the cloud. This individual will manage partnerships with telco partners & other vendors.
Essential Responsibilities
• Design large-scale secured & redundant connectivity solution over telecom service provider networks around the world to over all vision and requirements • Manage solution level requirements, scope and project implementation • Specify network architecture, network security, design and implementation. • Be a leading expert in the organization on developments with Industrial IoT connectivity solution, cloud-hosted infrastructure and platform services, software defined networking (SDN), and related connectivity solutions. • Collaborate with Network Principal Technologists (PT) and Subject Matter Experts (SMEs) to bring cutting edge networking technology to the environment by driving New Technology Introduction (NTI) efforts • Engage network deployment and operations organizations to successfully implement solutions and transition them into production • Mentor others to develop their technical expertise
Qualifications/Requirements
• Bachelor's Degree in Information Systems, Information Technology, Computer Science or Engineering from an accredited college or university or equivalent experience • Minimum 10+ years of experience in designing/supporting large enterprise network infrastructure and/or management solution • Minimum 5 years of experience in a primary design, architecture, implementation role in network infrastructure • 3+ years of Network infrastructure or datacenter security preferred.
Additional Eligibility Qualifications
GE will only employ those who are legally authorized to work in the United States for this opening. Any offer of employment is conditioned upon the successful completion of a background investigation and drug screen.
Desired Characteristics
• Demonstrated foundation of expertise in network fundamentals (routing, switching, TCP/IP, network security, etc) with a minimum of 10 years’ experience supporting large enterprise environments (Fortune 500) • Demonstrated training or experience with network virtualization, IaaS/PaaS, and/or SDN technologies including a minimum of 3 years designing solutions in these areas.
• Cisco Certifications such as CCNP, CCDP, CCIE, CISSP, CCDE. Similar certifications in areas related to network virtualization or internetworking solutions also a plus. • In-depth knowledge of network management, monitoring technologies – Nimsoft, Appdynamics, New Relic, Nagios etc, network availability, QOS & capacity planning. • Leverages communication and collaboration to solve problems with global peers across various functions. • Tenacious problem solver demonstrating an innate curiosity and passion for solving difficult problems. • Highly proficient and have extensive experience with IPSEC, IGP, MPLS, BGP, IPVPN, and TPC/IP technologies. • Experienced in designing networks and configuring network routing. • Experienced Cisco Engineer and Cisco Certified. • Working knowledge of a scripting language like Python or Ruby. • Familiarity with one or more of the following: Amazon Web Services, Rackspace, Openstack, datacenters, telecom circuits, or any other public or private cloud environment. • Effectively communicate to all levels of the organization including SMEs, Principal Technologist etc. • Experience in architecting and building highly available, highly scalable data centers. • Robust experience with IP addressing (IPv4 & IPv6), subnetting, routing, switching, VLANs, NAT, VPN, etc. #DTR
Section 19 Job
YOU MUST BE ABLE TO SATISFY THE REQUIREMENTS OF SECTION 19 OF THE FEDERAL DEPOSIT INSURANCE ACT.
Apply to job
&nbsp Save to cart
GE.com
|
Contact
Us
|
Candidate
Privacy Notice
|
Accessibility
|
Terms