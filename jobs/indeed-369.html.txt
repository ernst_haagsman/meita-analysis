Career Opportunities
Enterprise Account Executive
Location:
San Francisco, CA
Description
Company Overview
Zones, Inc. is a national provider of IT products, solutions and services to businesses, government, education and healthcare customers.  For more than 20 years, we have provided a single source for IT products and solutions that leverage the value of our deep experience and our equally deep commitment to our clients' success.  We distinguish ourselves by our commitment to providing Five Star Service which keeps our clients at the forefront of every decision we make.  We are committed to providing team members with the resources, tools and training to achieve success and excel in their professional growth.
The Role
Zones Field-based Enterprise Account Executives are experienced business development professionals who focus on driving revenue and margin growth for our mid-market and large Enterprise accounts.  Account Executives partner with Engineers, Solution Architects and
Software Licensing Sales Executives to develop deep, value add technology solutions for their customers.    With a
proven track record of selling technology solutions (storage, servers, software and professional services) to major customers, our Account Executives are trusted partners who manage deep account relationships, develop strategic sales plans and thrive on driving results.
Key Responsibilities
As part of Zones Enterprise Sales organization, drive new business at targeted Mid-Market and/or Large Enterprise accounts.
Deliver product and service revenue attainment selling hardware and software solutions while driving Advanced Technologies like Microsoft, Network Solutions, Unified Communications, Security & Mobility, and Virtualization.
Master Zones value proposition and consistently exceed revenue and profit goals; exceed solutions sales revenue and margin goals.
Build proactive, solid selling partnerships with Zones Engineers, Solution Architects and Software Licensing sales teams to drive solutions and software business in territory.
Manage account relationships through senior level engagements.  Develop penetrating sales strategies and pricing proposals; communicate and understand total cost of ownership, industry trends, and critical success factors to ensure effective customer rollouts.
Maintain strong partnerships with OEM partners such as Cisco, EMC, HP, Microsoft etc.
Engaged with extended Zones teams in responding to RFP’s, product transition plans, and services resource planning.
Position Requirements
5+ years of direct, field-based technology sales experience preferred;  VAR experience preferred
Professional services  selling  experience
Solid understanding of technologies and partners that drive Zones networking and storage solutions such as Cisco, EMC, HP, Microsoft etc.
Proven track record of hunting new business and marketing technical services; deep relationships in territory.
Bachelor's Degree in a related discipline; or equivalent experience
Ability to travel up to 25% as needed.
Optional Preferred Qualifications if Segment Specific position - Experience selling within
Commercial
or
Public Sector
as appropriate i.e. healthcare, retail or hospitality
We provide competitive compensation and a robust benefit package.  To make the most of your life at work and away from it, we offer comprehensive medical, dental and vision plans;  a 401(k) Plan with an annual discretionary match;  paid time off;  product discounts and fitness center options.
If you meet the qualifications above, are driven by results, and are looking for an exciting career in IT Sales or Sales Support, we want to hear from you!
Zones… Connecting Businesses and Technology…. Connecting People with Opportunity
We are proud to be an E/O/E and a minority owned business.
Are you a returning applicant?
Previous Applicants:
Email:
Password:
If you do not remember your password
click here .
Back to Search Results
New Search