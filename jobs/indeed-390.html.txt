Solutions Architect Sr
Global Home (change)
Canada
United States
Home
About Robert Half
About Robert Half
Corporate Careers
Staffing Careers
Featured Jobs
Accountemps Jobs
OfficeTeam Jobs
Robert Half Technology Jobs
Robert Half Finance & Accounting Jobs
Robert Half Management Resources Jobs
Robert Half Legal Jobs
The Creative Group Jobs
Corporate Jobs
View All Job Opportunities
Locations
Not finding a job?
Join Our Talent Community
View All Jobs
Robert Half on Facebook
Robert Half on LinkedIn
Robert Half on Twitter
Robert Half on Google+
Robert Half on Youtube
Search by Keyword
Search by Location
Share this Job
Email similar jobs to me
Please enable JavaScript to subscribe.
Solutions Architect Sr
Apply now »
Apply now
Email
Please wait...
Date:
Jun 2, 2015
Location:
San Ramon, CA, US
Company:
Robert Half
Req ID:
98461
Join one of the World’s Most Admired Companies!
Our corporate employees are the backbone of our operations and work with our teams around the world. Each employee at our corporate services locations plays a role in our company’s success.
If you want to make a difference — and work in an environment where you can thrive and innovate — apply for this job today!
Founded in 1948, Robert Half is the world’s first and largest specialized staffing firm and the parent company of Protiviti, a global consulting firm. We offer our clients a full spectrum of specialized staffing and consulting solutions through our more than 400 locations in over 20 countries. Robert Half has appeared on FORTUNE magazine’s list of “Most Admired Companies” every year since 1998, as well as numerous “Best Place to Work” lists around the world.
Job Summary
Robert Half is seeking a
Senior Solutions Architect
who will provide infrastructure architectural / solution support to project-based efforts for relevant technical areas, with the ultimate goals of increasing effectiveness, efficiency, and the success rate for these activities. Will design high-level enterprise systems solutions to address business needs using infrastructure system standards (servers, storage, networking, monitoring). Work with the business to understand requirements. Drive infrastructure design using the standards. Manage infrastructure deliverables to vertical/business. Investigates new technologies for potential roadmaps for the business/vertical and ultimately RH, in conjunction with the engineering teams. Work with the engineering teams to deliver new capabilities which could be leveraged by the business. Develop processes, standards, policies, and workflows to better document delivery methods and continually improve system delivery efficiencies.
Specific responsibilities will include:
Engage with project managers and project sponsors in the business to understand, clarify, and document the objectives and success factors for technical solutions that support business based project initiatives.
Leverage existing technical standards to design solutions for projects. Provide ownership of those designs for the lifespan of projects.
Assess future technologies and collaborate with the engineering teams to offer capabilities which can be utilized by the verticals.
Work with technical discipline owners and experts to maintain a clear understanding and awareness of current technical standards and direction; and to provide feedback on standards based on project experiences.
Design solution architecture artifacts and policies to improve predictability and efficiency of the solution architecture infrastructure solution delivery process.
Work with SA 1 to provide proper, thorough, and complete documentation that allows for the provisioning of new infrastructure related to projects and\or vertical-supporting systems.
Qualifications:
A Bachelor's degree in a technical field or comparable experience.
Must 7+ years’ senior-level experience with related technologies.
7+ years’ working in a large IT organization (200+ people).
5+ years’ working for a large corporation with multiple office locations.
7+ years’ as a Senior Engineer.
5+ years’ of project delivery experience in the creation and design of medium and large scale systems.
Experience as a Sr. Solutions Architect in a large IT organization.
Senior-level experience in architecture, installation, configuration, and maintenance of related technology.
Expert in integration of related technology with business and computer systems.
Expert in understanding, designing, implementing, and applying technology control processes (such as change management, production acceptance, etc.) to deliver project infrastructure needs.
Must be able to work in a highly dynamic and fast-paced environment.
Must be able to work independently as well as within a team both as a leader and as a team member.
Must have excellent documentation skills.
Must be able to convey knowledge to customers, peers, and managers of widely diverse skill levels.
Excellent customer service skills and customer focus.
Excellent communication skills.
Passion and drive for technology as a service and business enabler.
Follow us on Twitter
@RH_Corp_Jobs  for Robert Half Corporate job openings and career and workplace news!
Robert Half International Inc. is an Equal Opportunity Employer. M/F/Disability/Vet
As part of Robert Half’s corporate facility employment process, any offer of employment is contingent upon successful completion of a background check.
Nearest Major Market:
San Francisco
Nearest Secondary Market:
Oakland
Job Segment:
Solution Architect, Architecture, Engineer, Consulting, Facilities, Technology, Engineering, Operations
Apply now »
Apply now
Email
Please wait...
Find similar jobs:
Corporate San Ramon Jobs,
Corporate Jobs,
Consulting Jobs at Robert Half,
Change Management Jobs at Robert Half,
Architecture Jobs at Robert Half
Sign in
We noticed that you are already a member of our Talent Community. Please enter your password to continue.
Email*
Not you?
Password*
Forgot Password?
* required field
Submit
Cancel
Account Verification
We noticed you have accounts in our “Talent Community" and “Application" systems. We take security seriously and need to verify your identity to synchronize your accounts.
Account Email:
Please follow these steps to synchronize your accounts.
1. Reset your password:
Click here to reset your Password
The "reset password" link will open in a new browser window. Return to this page to enter your new password.
2. Enter your NEW password here:
Submit
Cancel
Already a Member
You are already a user of this site. Please sign in.
Email*
Password*
Forgot Password?
* required field
Sign in
Cancel
Start Your Application
Existing Users
Email*
Password*
Forgot Password?
New Users
Create a new account now!
Start Your Application
Receive Email Updates
Join our Talent Community
Sign in
Create
* required field
Create Profile
Working...
Next »
Terms of Use
Privacy
Contact Us
roberthalf.com
Top Job Searches
View All Jobs
socialmatcher Mobile
©
Robert Half  All Rights Reserved.
An Equal Opportunity Employer.