Mobility Architect Senior Associate Jobs in San Francisco at PwC
United States
[ X ]
Home
Why PwC
Career growth
Our culture
Our people
Employee benefits
Firm facts
Our markets
Firm recognition
Practice areas
Advisory
Assurance
Tax
Internal Firm Services
Public Sector practice
How we hire
News and media
Video gallery
Find your position
Job opportunity
PwC jobs
>
Senior Associate jobs
>
San Francisco Senior Associate jobs
Search jobs
Start your search by entering a keyword:
Search Jobs
Search jobs
Start your search by entering a keyword:
and/or select a category and location:
Enter Search Terms...
Category
Accelerated Hiring
Accounting Advisory Services
Administrative Services
Advisory
Analytics
Architecture
Assurance
Audit
Business Applications
Business Development
Capital Markets Services
Capital Projects and Infrastructure
CMAAS
Consulting
Corporate Finance
Customer Impact
Cybersecurity and Privacy
Deals
Digital Experience Center
Finance
Financial Due Diligence
Financial Instruments and Real Estate Valuation
Financial Services
Financial Statement Audit
Flexibility2 Talent Network
Flexible Location
Forensic Services
Global Assurance Delivery Model
Global Mobility Services
Health & Welfare Benefits Services
Health Advisory Services
Human Resource Services
Human Resources
Human Resources Services Consulting
IFRS
Information Management
Internal Audit
Internal Firm Services
International Tax Services
IT & Project Assurance
IT Infrastructure
IT Strategy
Learning & Development
Management Consulting
Marketing & Sales
Mergers and Acquisitions
Office General Counsel
Operations
Oracle
People & Change
Process Assurance
Program Management
Public Sector
PwC Saratoga
Recruiting
Regulatory Compliance and Reporting
Risk and Compliance
Risk Assurance Services
Risk Consulting
Risk Management
Salesforce.com
SAP
Seasonal Position
Security
Senior Associate
Shared Services Outsourcing
State and Local Tax Services
STEM
Strategy
Supply Chain Management
Sustainable Business Solutions
Tax
Tax Accounting Services
Tax Projects Delivery Group
Technology Consulting
The Carolinas Audit
Third Party Assurance
Transaction Services
Transfer Pricing Services
Valuation Services
Washington National Tax Services
Workday
Search By
Radius
Location
Maximum Distance
Miles
<--- Select --->
5
10
20
40
Enter Zip Code or City, State
State/Province
Alabama
Arizona
Arkansas
California
Colorado
Connecticut
District of Columbia
Florida
Georgia
Illinois
Indiana
Iowa
Kentucky
Louisiana
Maryland
Massachusetts
Michigan
Minnesota
Missouri
Nevada
New Jersey
New York
North Carolina
Ohio
Oklahoma
Oregon
Pennsylvania
Puerto Rico
South Carolina
Tennessee
Texas
Utah
Vermont
Virginia
Washington
Wisconsin
City
Albany
Atlanta
Austin
Baltimore
Beavercreek
Bedminster
Bentonville
Birmingham
Boston
Buffalo
Cambridge
Charlotte
Chicago
Cincinnati
Cleveland
Columbia
Columbus
Dallas
Dayton
Denver
Des Moines
Detroit
Falls Church
Florham Park
Fort Lauderdale
Fort Worth
Franklin
Grand Rapids
Greensboro
Harrisburg
Hartford
Houston
Indianapolis
Irvine
Jacksonville
Jersey City
Kansas City
Las Vegas
Lexington
Lithia Springs
Little Rock
Los Angeles
Louisville
McLean
Melville
Miami
Milwaukee
Minneapolis
Montpelier
New Orleans
New York
Ogden
Oklahoma City
Orlando
Peoria
Philadelphia
Phoenix
Pittsburgh
Portland
Raleigh
Richmond
Rochester
Rosemont
Sacramento
Salt Lake City
San Diego
San Francisco
San Jose
San Juan
Seattle
South Ogden
Spartanburg
Springdale
St. Louis
Stamford
Tampa
Toledo
Tulsa
Washington
West Palm Beach
advanced search
Job details
Culture
Location
Apply now >
Send to email >
Mobility Architect Senior Associate
Line of Service:
Advisory
State & City:
WA-Seattle
TX-Houston
TX-Dallas
PA-Philadelphia
NY-New York
NJ-Florham Park
MA-Boston
IN-Indianapolis
IL-Chicago
GA-Atlanta
DC-Washington
CA-San Francisco CA-San Diego Travel Requirements:
81-100%
Position Type:
Full Time Auto req ID:
64865BR-1 PwC/LoS Overview:
Are you interested in the opportunity to work for an industry-leading firm that services clients that include the Fortune 500, and will give you the experience and exposure you need to build your career and personal brand? If you are, then PwC US (PricewaterhouseCoopers LLP and its subsidiaries) may be the firm for you.
We're a member of the PwC network of firms located in 157 countries with more than 184,000 people who are committed to delivering quality in assurance, tax and advisory services. PwC US helps organizations and individuals create the value for which they are looking.
Our professionals are at the heart of our business strategy and success by bringing personal and professional experiences; we understand that our business is impacted by a person's personal and professional lives.
PwC US recruits top talent with traditional and nontraditional backgrounds, with a focus on diversity and inclusion, so that we continue to build PwC US as a great place to work. Our people are armed with the tools-including enriching professional experiences, everyday coaching, timely and productive feedback, and high-quality learning and development opportunities-to deliver each day. We are committed to building lasting relationships and delivering value to our clients. Learn more about us at www.pwc.com/us From strategy through execution, PwC Advisory helps clients build their next competitive advantage. As the world's second largest global consulting provider, we combine the breadth of knowledge of over 35,000 global professionals with deep industry knowledge to deliver custom solutions for our clients. A long history of working with many of the world's largest and most complex companies means we really understand the unique business challenges our clients face better than most consultancies. Job Description:
Across industries, our clients are focused on improving business performance, responding quickly and effectively to crisis situations, and extracting value from transactions. Our growing Consumer Industrial Products & Services (CIPS) practice provides management, technology and risk consulting services to help a diverse set of clients around the world anticipate and address their most complex business challenges. CIPS includes multiple sectors: Automotive, Energy, Industrial Products, Retail & Consumer, and Utilities. Our Applications Strategy and Integration consultants provide an end to end solution offering including Application Development & Integration, Application Architecture, User Experience, Quality Management & Testing and
helping clients determine the best applications for their business needs and integrate new and existing applications into their business including Mobility integration.
This high performing team works with various types of technologies and software framework including Java, .Net, iOS, HTML5 amongst others. This high performing team advises on the software development life-cycle, and help architect and develop specific applications for clients. Additionally, they assist with integration of new applications and quality management and testing. Position/Program Requirements:
Minimum Years of Experience: 4 Minimum Degree Required: Bachelor's degree Knowledge Preferred: Demonstrates proven thorough knowledge and interaction with end users to understand and document business and functional requirements for complex projects, as well as collaborating with technical teams across the full software development life cycle.
Demonstrates considerable proven knowledge of analytical, organizational, planning and project management specific to software development lifecycle processes, which may include the following: -Utilizing n-tier development, SDLC and Object-oriented design. -Designing and implementing technology for new or existing business applications. -Utilizing problem-solving know-how, especially debugging and troubleshooting complex software orchestrations, and identifying solution options and/or alternatives. -Scoping and estimating project tasks, as well as managing multiple tasks with minimal supervision. Skills Preferred: Demonstrates proven thorough abilities that include creative thinking and problem solving skills, individual initiative and the following abilities:
-Conducting quantitative and qualitative analyses of large and complex data. -Leading a key workstream in an engagement(s), staying educated on current trends and assisting in the development of knowledge capital. -Collaborating with business development teams responsible for writing and presenting proposals to prospective clients. -Supporting practice management for a specific operation or process. -Managing and/or contributing to project planning, engagement administration, budget management, and successful completion of engagement workstream(s). Demonstrates proven extensive abilities and success performing on technical teams across the full software development life cycle, including the following areas -Leveraging commerce platforms (such as Hybris, Micros, Oracle ATG, Demandware, IBM or similar) -Leveraging multiple platforms, tools and custom extensions as a Solution architect, including: User Experience and Front End Presentation Layer (HTML5/CSS3, JS/AJAX, Responsive Design), Back-end and middleware development (JEE frameworks and design patters like spring, Sling, etc.), -Utilizing Enterprise Architecture Integration (EAI) technologies: IBM WebSphere, BEA Weblogic, Webmethods, TIBCO, SeeBeyond, Biztalk etc. Architecting cloud (SaaS, PaaS, IaaS, BaaS) operations, delivery and brokerage services. -Providing architecture know-how on E-Commerce transformation projects to optimize streamline and improve capabilities across content management, payment, promotions and fulfilment. -Leading e-commerce and multichannel/Omnichannel initiatives, including digital development, user experience, organizational change and technology. -Architecting SOA applications and implementing custom solutions with Spring/Hibernate framework and emerging Web 2.0 frameworks, XML and XML modelling, Data Modeling and Architecture experience with Oracle/DB2 and/or NoSQL environment. -Leveraging TIBCO Product Suite, Storage Systems, Content management, Order management, Product information management, Digital asset management, Search & merchandising
-Utilizing the full life cycle development J2EE project and related development . -Performing as a team member: understanding personal and team roles; contributing to a positive working environment by building solid relationships with team members; proactively seeking guidance, clarification and feedback; providing guidance, clarification and feedback to less-experienced staff.
Job ID: 64865BR-1
Culture
Location
Join our talent community
Sign up and manage job alerts for PwC and you'll receive an email when new positions become available. To add more than one keyword, simply enter the keywords separating each with a space. Keywords Location Email Success! Please check your email to activate the job alert. To subscribe please enter two or more letters for Keyword
Share this job
Connect with us
Useful links
Facts about the firm
PwC careers
Corporate responsibility
pwc.com/campus
Diversity at PwC
Jobs for vets
Information for individuals with disabilities
Press room
Alumni
RSS
Other sites
US offices
Contact us
© 2008-2014 PricewaterhouseCoopers LLP, a Delaware limited liability partnership. All rights reserved. PwC refers to the United States member firm, and may sometimes refer to the PwC network. Each member firm is a separate legal entity. Please see
www.pwc.com/structure
for further details.
Privacy
Legal
Site provider
Site map
Facebook
Twitter
LinkedIn
YouTube
Instagram
If you are a human, do not fill in this field.