import nltk

grammar = r"""
    NP: {<CD|TO>+<NN.?|JJ>*}
"""

string = ' Requirements : Typically seven to ten years professional experience in a'

tokens = nltk.word_tokenize(string)
cp = nltk.RegexpParser(grammar)
#tagged = nltk.ne_chunk(nltk.pos_tag(tokens))
tagged = cp.parse(nltk.pos_tag(tokens))

print tagged