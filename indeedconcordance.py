from __future__ import print_function
__author__ = 'eh'


import mysql.connector

import nltk
from nltk.text import Text
import sys

concordance_string = sys.argv[1]

# load jobs

cnx = mysql.connector.connect(user='analysis', password='1234', host='127.0.0.1', database='meita-jobs')
cursor = cnx.cursor()

query = "SELECT pagetext FROM indeedjobs"
cursor.execute(query)

job_descriptions = []

for (job) in cursor:
    job_descriptions.append(job[0])

descriptions = '\n'.join(job_descriptions).encode('ascii', 'ignore')

tokens = nltk.word_tokenize(descriptions)

textobj = Text(tokens)

textobj.concordance(concordance_string, width=120, lines=1000)
