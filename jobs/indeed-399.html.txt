Emdeon Careers
Providers
Payers
Pharmacy Services
Channel Partners
Dentists
Resources
Payer Lists
Customer Login
Enrollment Forms
HIPAA Simplified
Resource Library
Tradeshows & Events
Speakers
Contact Us
eNewsletters
Follow Us
RSS Feed
Facebook
Twitter
Digg
About Us
Newsroom
Investors
Careers
Login
Careers
Company Overview
Great Benefits
Our Culture
Explore Locations
Browse Jobs
Frequently Asked Questions
Browse Open Jobs
|
Search Open Jobs
|
Submit Your Resume
|
Edit Your Profile
Job Details
Requisition Number
14-1877
Post Date
5/26/2015
Title
Enterprise Architect, Payments
City
San Mateo
State
CA
Description
Position Purpose
The Enterprise Architect is responsible for IT technical strategy as the Payments Development organization’s top design resource. Advises senior leadership on matters of technical strategy. They provide a high level of architectural experience and advice to the technical staff. Ensures the alignment of the organization’s mission, strategy and processes to the overall IT and Product strategy using a variety of architectural models. Expands the company’s use of technology as a strategic enabler of goals and objectives.
The Enterprise Architect is personally responsible for the most important and complex projects within Payments Engineering and Development. Involved in the development of policies, standards and guidelines that direct the selection, development, implementation and use of information technology within the organization.
Principal Accountabilities
• Understands the requirements for technology and system solutions as key enablers to achieve business objectives and advises the business
• Advises key business and IT stakeholders on the merits/recommendations of specific architectural solutions
• Provides technical and engineering expertise in all areas of system architecture and design to include:
• Physical, logical and conceptual
• All architectural domains including but not limited to data, integration, networking, security and application
• Responsible for the Enterprise Technical Architecture and Standards for both hardware and software
• Responsible for solution architectures for systems with significant scope in technology assets used
• Provides important technical and general coordination/interface with business and technical stakeholders for IT projects
• Approves system and solution architectures submitted by business-specific IT units recommending changes and/or new implementations where solutions are inconsistent with the Enterprise Technical Architecture or best practices for architectural design
• Creates and maintains the Enterprise Technical Architecture and Standards (e.g., technology road map and standards)
• Leads and directs new technology evaluations and selections and reviews requests for significant new footprint of existing technologies
Key Performance Indicators
• Business-specific IT units understand and are able to conform with the Enterprise Technical Architecture and Standards
• All project related tasks and deliverables are performed and delivered on schedule and within budget
• Architectures developed or reviewed are able to be implemented within budget and time as defined by the business
• Technology costs for solutions are aligned with the objectives of the business
Requirements
Education and Experience
• Bachelor’s degree in computer and information sciences
• Master’s or MBA (preferred)
• Fifteen+ years of experience in information technology
• Eight+ years of hardware and software architectural design using modeling techniques and tools
• Two+ years of experience in healthcare information technology (preferred) Required Knowledge and Skill
• Knowledge and understanding of business process, objectives, milestones and structure
• Knowledge and understanding of corporate policies and business strategy
• Demonstrated ability to communicate (written and orally) and influence at all levels
• The ability to produce, manipulate, implement or enhance Enterprise Technical Architecture and Standards concepts, roadmaps, principles or effective strategies using a variety of tools
• The ability to identify and describe the relationship between logical and physical entities and components; entities include business functions, business process, organizational structure, business systems, computing capabilities and technical infrastructure
• Demonstrated knowledge of data center infrastructure design and management
• Demonstrated knowledge of EA frameworks such as TOGAF, Zachman or FEAF
• The ability to constructively question established process
• Expert knowledge with IT systems and technologies
• All architecture types: business, information, technical and solution
• All architectural domains including data, integration, security, application and infrastructure
• Broad knowledge of healthcare information technology business processes and industry standards
• Broad knowledge of common business and IT best practices and procedures, including but not limited to
• IT service design and management
• Configuration, change, risk and release management
• Deployment and operations management
• Business continuity and disaster recovery
• Service level agreements (SLAs)
#LI-PD1
Emdeon is an Equal Opportunity Employer. Employment at Emdeon is based upon your individual merit and qualifications. We don’t discriminate on the basis of race, color, religion, gender, sexual orientation, gender identity or expression, national origin,
age, physical or mental disability, marital status, protected veteran status or disability, genetic characteristic, or any other characteristic protected by applicable federal, state or local law. We will also make all reasonable accommodations to meet our
obligations under the Americans with Disabilities Act (ADA) and state disability laws. All qualified applicants will receive consideration for employment without regard to race, color, religion, sex, national origin, protected veteran status, or disability. Please vist Equal Employment Opportunity Posters provided by OFCCP
here .
Apply On-line
Send This Job to a Friend
About Emdeon
Our Company
Newsroom
Investors
Careers
Contact Us
Customer Login
Additional Resources
US Healthcare Efficiency Index
HIPAA Simplified
Commercial Billing & Payment Solutions
Emdeon eNewsletters
Emdeon Locations
Industry Involvement
© Copyright Emdeon Business Services LLC, 2005-
- All Rights Reserved
Site Map
|
Privacy Policy