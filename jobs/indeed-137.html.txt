Chief Infrastructure Architect – Intuit Careers
Home
Meet Intuit
Introducing Intuit
Bold Environment. Innovative Technology.
Inspired Challenges. Meaningful Careers.
Awesome People. Powerful Teams.
Let's talk.
Jobs
Design & User Experience
Product Management
Software Engineering
Administration
Corporate Strategy & Development
Customer Service & Support
Data
Executive
Finance & Operations
Human Resources
Interns & Early Career
Information Technology
Legal
Marketing
Mobile
Process Excellence
Project & Program Management
Real Estate & Workplace Services
Sales
Tax Support
Log in to your profile
Intuit Careers
Log in to your profile
Recommended For You
Menu
Meet Intuit
Introducing Intuit.
Bold environment. Innovative Technology.
Inspired challenges. Meaningful careers.
Awesome people. Powerful teams.
Let's talk.
Jobs
Design & User Experience
Product Management
Software Engineering
Administration
Corporate Strategy & Development
Customer Service & Support
Data
Executive
Finance & Operations
Human Resources
Interns & Early Career
Information Technology
Legal
Marketing
Mobile
Process Excellence
Project & Program Management
Real Estate & Workplace Services
Sales
Tax Support
Recommended for you
Apply now
Email job
‹
Go Back
Chief Infrastructure Architect
00116115
Locations:
Menlo Park, California
Description   It’s an exhilarating time to be a member of the Product Infrastructure group at Intuit.  In the PI organization we design, build and deliver several large, web-scale environments to enable and propel a multitude of online products. These include known and meaningful applications such as Mint, Turbo Tax, Quickbooks Online- the small business operating system, and many more.  The  Chief Infrastructure Architect  is
a senior technical leadership role within the Technology Organization. The
right candidate will have broad infrastructure expertise, software skills, a solid
understanding of software development challenges, a working knowledge of
operations for services that are deployed in multi-party environments, a base
understanding of secure operations, and have a track record of architecting
transformational infrastructure solutions.
Ensure Business
and Technology Alignment .  Ensure alignment of
Infrastructure Architecture and Investments with CTO and Enterprise
Architecture vision.
Own the Infrastructure
Strategy :
Continuously evaluate current environment, assess capability gaps and
define target state. Identify and own the roadmap to target state. Develop
business cases for strategic investments in Infrastructure. Lead Infrastructure
Architecture function and strategy :  This includes all aspects
of product supporting infrastructure technology including OS,
infrastructure software, systems and services monitoring, systems
management, end-to-end automation storage, data center, network, and
capacity management. Directly Manage a
small group of architects  who design solutions for specific
products or infrastructure areas Bridge Product
Infrastructure and Product offerings:
Work with senior technical staff
in Business Units and Functional groups to understand needs of offerings
and provide guidance on infrastructure.
Standards
Management :
Work with development, architecture and operations teams to set the
standards, patterns and reference architectures. Define strategies for resiliency,
disaster recovery. Manage exceptions to standards and drive closure of
exceptions.  Emerging Trends
Evaluation :
Engage with internal and external partners to identify emerging trends in
infrastructure technology, automation, operations and security and help
align to business needs Leadership and
Talent Development : Act as role model and mentor for architects and
technical managers
Qualifications   Qualifications
BS/MS in computer science or
equivalent work experience. 15+ years’ experience designing
and leading architectural efforts for infrastructure, web and SaaS  applications
Seasoned leadership experience:
Applying the nuances of influencing based on the situation, knowing when
to push and when to ease, driving through designs, ideas, and roadmaps. Excellent communication skills:
Demonstrated ability to present to all levels of leadership, including
executives. Expert in the architecture
process. Experience in conceptualizing,
launching, and driving business and product strategies and multi-year
roadmaps. Building strong teams - set and
evangelize vision, facilitative leadership, attract and retain key talent. Expertise with Agile
Development
Imagine a career where your creative inspiration can fuel BIG innovation.  Year-over-year, Intuit has been recognized as a best employer and is consistently ranked on Fortune’s “100 Best Companies To
Work For” and Fortune World’s “Most Admired Software Companies” lists. Immerse
yourself in our award winning culture while creating breakthrough solutions
that simplify the lives of consumers and small businesses and their customers worldwide.
Intuit is expanding its social, mobile, and global
footprint with a full suite of products and services that are revolutionizing
the industry.  Utilizing design for
delight and lean startup methodologies, our entrepreneurial employees have brought
more than 250 innovations to market – from QuickBooks®, Quicken®, and
TurboTax®, to GoPayment, Mint.com, big data, cloud (SaaS, PaaS) and mobile
apps.  The breadth and depth of these
customer-driven innovations mean limitless opportunities for you to turn your
ingenious ideas into reality at Intuit.
Discover what it’s like to be part of a team that
rewards taking risks and trying new things. It’s time to love what you do!
Check out all of our career opportunities at: careers.intuit.com.  EOE AA M/F/Vet/Disability
Not for you? Check out these other opportunities:
Staff Network Engineer
•
Staff Network Engineer
•
Staff Network Engineer
•
Senior Network Engineer
•
Senior Network Engineer
•
See More…
Apply now
Apply now
Email job
You Can Also
Keep up to date with Intuit
Share this job
See LinkedIn Connections
Related Careers
Software Engineering
Data
Meet Intuit
Introducing Intuit.
Bold Environment. Innovative Technology.
Inspired challenges. Meaningful careers.
Awesome people. Powerful teams.
Let's talk.
Jobs
Design & User Experience
Product Management
Software Engineering
Administration
Corporate Strategy & Development
Customer Service & Support
Data
Executive
Finance & Operations
Human Resources
Interns & Early Career
Information Technology
Legal
Marketing
Mobile
Process Excellence
Project & Program Management
Real Estate & Workplace Services
Sales
Tax Support
Intuit
© 2015 Intuit Inc. All rights reserved. Intuit is an Equal Employment Opportunity Employer that is committed to inclusion and diversity. We also take affirmative action to offer employment and advancement opportunities to all applicants, including minorities, women, protected veterans, and individuals with disabilities. If you'd like more information about your EEO rights as an applicant, please
click here .
Legal
·
Privacy
·
Security
Search Intuit Careers
(i.e. design, manager, cloud, req#…)
×
Or
Select a role
Select a location
jobs match your current search
-
Reset
Stay up to date on new opportunities and the latest Intuit news and events. Join our
Talent Network .
Log in
to your profile to check the status of or complete an application.
Reset
sort by
search results loading
{% job.secondary_title %}
See more
See who you already know at Intuit.
×
×
×
×
sort by
Design & User Experience
Product Management
Software Engineering
Administration
Corporate Strategy & Development
Customer Service & Support
Data
Executive
Finance & Operations
Human Resources
Interns & Early Career
Information Technology
Legal
Marketing
Mobile
Process Excellence
Project & Program Management
Real Estate & Workplace Services
Sales
Tax Support
×
×