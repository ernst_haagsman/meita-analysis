Redolent, Inc - Software solution company - Web, Open source, e-commerce, QA services, Offshore development
Sr / Staff Software Engineer (Java)
Employee Login  |
Customer Login  |
Feedback
Follow us on
Home
About Us
Overview
Mission
Why Redolent
Corporate Values
News
Careers
Solutions
Web Applications
CRM
E-Commerce
Opensource Development
Quality Assurance
SaaS Solutions
Google Apps
Cleantech Solutions
Offshore Development
IT Staffing Solutions
Training Solutions
Our Methodology
Portfolio
View Projects
Technology Domains
Industry Domains
Vertical Domains
Products
Clanio
E-TalentGenie
CodeSire
red-FPM
PHP-RAD
Cool Tools
Clients
Client List
Case Studies
Customer Quotes
Partners
Become Partner
Contact Us
Offices
Feedback
Request Information
Project Estimate
Careers
»
Open Jobs
»
Register profile
»
Why Join Redolent
Home
>
About Us
>
Open Jobs
Jobs
Register
(Optional)
Log In
Back to Jobs
Sr / Staff Software Engineer (Java)
Location:
San Francisco, CA
Job #
4186245
Date Posted: 09-02-2014
Apply Now
Send to Friend
TITLE: Staff Software Engineer (Java)
Location: San Francisco, CA
Duration: Full time
Compensation: Competitive ( DOE )
JOB SUMMARY:
We are looking for talented and passionate software engineers who love to code and aren’t intimidated by
challenging problems to create impactful technology.
Our problem space includes everything from capturing and exploring huge performance related datasets
to presenting complex information in simple ways, empowering businesses with actionable intelligence
for scaling their services.
QUALIFICATIONS and EXPERIENCE:
•  B.S/MS in Computer Science or related curriculum with 5-10 years experience in platform or application software development and design
•
Strong Java development skills with deep familiarity of design patterns
• Mastery of JavaScript applications including server-side
(Node.js) programming
•
Strong
Python
skills
• Expertise in
J2EE, Java Web Services (REST/SOAP), JAXB, JPA, Hibernate
and various caching and scaling technologies and strategies for enterprise level web applications
• Strong familiarity with Queue and Messaging in an event driven architecture
• User level proficiency in SQL (MySQL, Oracle) and NO-SQL (MongoDB) databases
• Solid experience in agile development process and possibly scrum master
Desired skills:
• Experience in
Amazon EC2, S3, EBS or related technologies
is not a requirement but a great plus
• Familiarity with functional programming,
Django, d3.js
• Familiarity with website performance measurement techniques
• Mobile App development experience would be a plus
RESPONSIBILITIES:
• Be a passionate engineer - build secure, highly scalable, reliable and high performance distributed
systems.
• Drive the product life cycle from strategic requirements prioritizing to tactical value delivery.
• Collaborate with architects, product managers, peers, technical writers, and QA engineers shaping
the product future by taking part in product feature, architecture and design decisions.
Apply Now
Send to Friend
Terms of use  |
Privacy Policy  |
Site map  |
Feedback
© All Rights Reserved.
Redolent, Inc, USA | 2009
Redolent Infotech Pvt. Ltd, INDIA | 2009