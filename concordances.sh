#!/bin/sh

rm -rf ./out/
mkdir out
python indeedconcordance.py legal > out/legal.txt
python indeedconcordance.py experience > out/experience.txt
python indeedconcordance.py leadership > out/leadership.txt