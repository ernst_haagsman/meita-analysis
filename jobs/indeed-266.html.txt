Paxata - Solution Architect Solution Architect Redwood City Customer Success Full Time Apply to this job With Paxata, our customers are securing our borders, tracking down money launderers, and delivering fresh yogurt to your local grocery. Pioneering the idea that you should not have to write MapReduce jobs to run a data-driven organization, Paxata empowers anyone to transform large, raw data sets into useful AnswerSets that fuel insight generation in ways not previously possible.
Innovation moves quickly on our technology platform that consists of semantic algorithms, Apache Spark, awesome UI, and a cloud architecture. The problems we solve are challenging and fun, like dynamically increasing our data capacity by an order of magnitude and enabling users to intuitively command complex data solutions. Our business opportunity is huge and our impact is real -- in just a year since launch, over 150 Pax Power users across financial services, government, and consumer industries solve data problems with Paxata.
We are a tight-knit team of experienced and diverse problem solvers with track records of writing complex software, closing deals, and delivering customer success. We value results and learning and welcome you to join us in building the next great enterprise software company. If you want to build ground-breaking products, transform the lives of customers, and drive a culture of results and confidence, this is the team for you.
Founded in May 2012, Paxata is headquartered in beautiful downtown Redwood City, CA (by the Caltrain station). www.paxata.com, @paxata_news. What you would be doing Participate in both the pre and post sales
processes, assisting sales and services to analyze and architect how Paxata fits
into a complex Hadoop® ecosystem
Engage with IT and Business executives
of prospective or current customers for in-depth discussions about:
1) Paxata's compatibility with any new or existing platforms
2) How Paxata works with their enterprise Big Data strategies (including
“classic” relational, warehouse, and NoSQL) Work with prospective or current IT
customers in Paxata installation planning and administration, including: 1) Installation / upgrade procedures, proper hardware sizing /
cluster configuration, and
2) Performance tuning and troubleshooting Build and deliver architecture
presentations and / or whiteboard sessions to end-users and prospects
Work closely with engineering and
product management to document and build architecture planning and installation
best practices, including technical collateral and guides
Serve as interface with Cloudera
(corporate partner) on overlapping installations Requirements
Ability to understand
“Big Data” use cases and what it means for both compute and storage resources
in order to recommend the appropriate server and storage required in a Hadoop
deployment  Hands-on with “Big Data” technologies
with the Hadoop stack (e.g. MapReduce, Pig, Hive, HBase) and practical
experience that allows you distinguish between the implementation reality and the
hype Strong experience implementing
application software in an enterprise Linux environment  Good understanding of RDBMS,
JDBC/ODBC, Integration Technologies, ETL and System Architecture  Minimum of a Bachelor’s degree in
Computer Science, Software Engineering, Information Systems or related field;
or equivalent experience Minimum 4 years of client-facing
experience in technical consulting or data analytics professional
services Strong oral and written communication
skills; excellent presentation skills
Bonus (“Nice-to-Haves”)
Background in
vertical industries such as financial services, government, CPG, and/or
high-tech manufacturing  Experience with DQ and MDM   Familiarity with analytics
applications, such as Tableau, QlikView, Spotfire, GoodData, and/or
MicroStrategy  Experience working with Apache Spark
Apply to this job Paxata Home Page Jobs powered by