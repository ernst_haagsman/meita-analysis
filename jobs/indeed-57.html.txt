Data Architect - Buxton Consulting - San Francisco, CA - 06-16-2015 | Dice.com
Toggle navigation
Dashboard
Search
Tech News
Post Jobs
Sign In
Data Architect
Buxton Consulting ,
San Francisco, CA
Posted 3 hours ago
Save
Email
Share
Toggle
Dropdown
Email
Enterprise Data Architect /Data modeler
Full Time, Contract Independent, Contract W2, C2H Corp-To-Corp, C2H Independent, C2H W2, Part Time, long term
open
Travel not required
Telecommuting not available
Job Description
SUMMARY The Enterprise Data Architect (EDA) will be responsible for developing and implementing an enterprise data strategy that aligns enterprise information assets with business and technology strategy. This will include documenting and rationalizing multiple data repositories and organizing them into a logically networked, highly-scaled, enterprise data warehouse (EDW) containing clinical, financial, operational, research and educational data. The EDA will enable the efficient and actionable use of a data across the client’s Enterprise and across the client's health system 14 million patients.   The Enterprise Data Architect will work with EDW and Analytics management, data governance staff, primary and secondary data owners, and BI and data management professionals to: Define and implement a data strategy and roadmap for client Define policies and standards for modeling, structuring, naming, describing, securing and formatting data Define and document the data architecture for enterprise data assets, working with business and IT stakeholders Develop conceptual, logical and physical data models with associated metadata including data lineage and technical data definitions Undertake data modeling, sourcing and profiling in support of specific enterprise and customer-facing data-driven initiatives Design and implement data warehouses, marts, cubes and universes to support analytic work product requirements Lead efforts to implement a reference data and master data management plan Build a centralized data-architecture practice in support of strategic business and IT goals   The Enterprise Data Architect will stay abreast of current and emerging data management technologies and trends and advise on how they can be used to support business and technology objectives. The Enterprise Data Architect will create a framework in information assets for multiple business domains can be delivered in a consistent, standards-based framework. The successful candidate will have leadership skills, be customer focused and have extensive experience in developing enterprise data architecture solutions. REQUIRED QUALIFICATIONS An undergraduate or master’s degree in Computer Science, Information Systems, Engineering or related field 10+ years’ experience in healthcare or related-field data or database work 5+ years’ experience as a Data Architect or Data Modeler in the healthcare or related industry Expertise in the fields of data quality, data profiling, data security, Master Data concepts, and data migration Demonstrated expertise with data modeling (relational, multi-dimensional, columnar, etc.), architecture and documentation software and tools such as ERwin and ERStudio Broad, working knowledge of MS SQL, Oracle, and other DB systems Working knowledge of information security best practices and demonstrated commitment to information security Strong leadership, interpersonal, influencing, collaboration and negotiation skills Ability to communicate with high proficiency, both verbally and in writing, with all levels of management and staff, in both technical language and layman’s terms Ability to work independently with minimum supervision Able to prioritize effectively according to department needs, and ability to organize a large number of changing variables Ability to assess and solve complex technical problems Experience in developing technical staff   PREFERRED Master’s degree in Computer Science or Information Systems 8+ years as a data architect or data modeler Experience with IBM InfoSphere Experience with Big Data and Hadoop technology Experience with Epic Clarity and Cogito Experience with financial, operational, HR, payroll and research data Experience in big data platforms   Please contact gkrishna @ us-buxton.com
Posted By
6140 Stoneridge Mall Rd., Suite 100
Pleasanton, CA, 94588
Buxton Consulting,
Dice Id : buxton
Position Id : 083892
Similar Positions
Data Architect
AB2 Consulting Inc
San Ramon, CA
Data Architect
Collabera
Foster City, CA
Data warehouse Architect
Technology Resource Group
Palo Alto, CA
View more jobs like this
Data Warehouse Architect
Nityo Infotech Corporation
Palo Alto, CA
Solution Architect, Business Intelligence
TRIAD Group
San Francisco, CA
Data Architect
Compucom
Foster City, CA
Big Data Architect
AKVARR
San Ramon, CA
Oracle MDM Architect Needed
dotSolved
Belmont, CA
Senior Data Engineer- 2 positions
4C Infotech LLC
Redwood City, CA
Data Modeler with Pharma domain projects exp. || San Francisco, CA
E-Solutions, Inc.
San Francisco, CA
View less jobs like this
×
Email Job
Your Email
Recipient's Email
CC this e-mail to me
Message
Send
Dice
Learn About Dice
About Us
Company Profile
Contact Sales
Social Recruiting
Work at Dice
Privacy Policy
Contact Us
Dice Services
The Dice Report
Open Web
Browse Jobs
Skills Center
Dice Sites
Slashdot
SourceForge
The IT Job Board
Dice Everywhere
Twitter
Facebook
Google Plus
App Store
Google Play
Copyright ©1990 - 2015 Dice. All rights reserved. Use of this site is subject to certain
Terms and Conditions .
Dice is a
DHI Service
×
It's a little rough around the edges. If you find things we could do better, let us know at
newdicesupport@dice.com
or give us a call at 888-321-DICE (3423)
Continue with the new site